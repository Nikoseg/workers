const apiURL = 'https://api.workers.com.ar';
//const apiURL = 'http://localhost/workers_api';
//const apiURL = 'http://www.workers.local';

// DEV
if(process.env.NODE_ENV === 'development') {
    // Podemos despues ajustar la apiURL acá...
}

const apiVersion = '/v1/';

export const apiURL_user = apiURL + apiVersion + 'users/';

export const apiURL_workers = apiURL + apiVersion + 'workers/';

export const apiURL_workers_cuil = apiURL_workers + 'cuil/';

export const apiURL_services = apiURL + apiVersion + 'services/';

export const apiURL_search = apiURL_workers + 'search/';

export const apiURL_companies = apiURL + apiVersion + 'companies/';

export const apiURL_companies_me = apiURL_companies + 'me/';

export const apiURL_qualifications = apiURL + apiVersion + 'qualifications/';

export const apiURL_categories = apiURL_workers + 'categories/';

export const apiURL_user_me = apiURL_user + 'me/';

export const apiURL_user_profile_image = apiURL_companies + 'profile_image/';

export const apiURL_worker_profile_image = apiURL_workers + 'profile_image/';

export const apiURL_get_service_areas = apiURL_workers + 'areas/';

export const apiURL_auth = apiURL + apiVersion + 'auth/';

export const apiURL_password_request = apiURL_user + 'password_request/';

export const apiURL_location_states = apiURL + apiVersion + 'locations/states/';

export const apiURL_location_cities = apiURL + apiVersion + 'locations/cities/';
