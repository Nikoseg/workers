import { userDataLocalStorage } from './../models/constants'
import {headerNavId, setNavLinksEvent} from "../models/constants";
import './../modules/apiService';
import APIService from "./apiService";

class AuthService {

    constructor(){
        this.api = new APIService();
    }

    async login(email, password) {
        const loginResponse = await this.api.login(email, password)
            .then(res => {
                this.saveUserData(res);
                this.setHeaderInOrderOfUserStatus();
            }).catch(err => {
                throw new Error(err);
            });

        return loginResponse;
    }

    async updateUser(obj) {
        const updateUserResponse = await this.api.updateUser(obj)
            .catch(err => {
                throw new Error(err);
            });

        return updateUserResponse;
    }

    async passwordRequest(email) {
        const passwordRequestResponse = await this.api.passwordRequest(email)
            .catch(err => {
                throw new Error(err);
            });

        return passwordRequestResponse;
    }

    changePassword(obj){
        return new Promise((resolve, reject) => {
            if(!obj || !obj.oldPassword || !obj.newPassword || !obj.newPasswordRepeated){
                reject(new Error('obj no definido'));
            }

            if(obj.newPassword !== obj.newPasswordRepeated){
                reject(new Error('Las contraseñas no coinciden'));
            }
            const userData = this.getUserData();

            this.login(userData.email, obj.oldPassword).then(()=> {
                let data = {
                    id: userData.id,
                    password : obj.newPassword,
                    name: userData.name
                };
                this.updateUser(data).then((res)=>{
                    resolve(res);
                }).catch((err) => {
                    reject(err);
                });
            }).catch((err)=>{
                reject(err);
            });
        });
    }

    logout(redirect) {
        localStorage.clear();
        this.setHeaderInOrderOfUserStatus();

        if(redirect) {
            window.location.href = '/login'
        }
    }

    saveUserData(res) {
        localStorage.setItem(userDataLocalStorage, JSON.stringify(res));
    }

    getUserData() {
        return JSON.parse(localStorage.getItem(userDataLocalStorage));
    }

    checkLoginStatusAndDoSomethingOrDefault(doSomething, _default) {
        const userData = JSON.parse(localStorage.getItem(userDataLocalStorage));
        userData && userData.token ? doSomething() : _default();
    }

    setHeaderInOrderOfUserStatus() {
        const nav = document.getElementById(headerNavId);
        if(!nav) { throw new Error('Could not find the header navigation'); }

        const setNavLinksEvt = new Event(setNavLinksEvent);
        nav.dispatchEvent(setNavLinksEvt);
    }
}

export default AuthService;