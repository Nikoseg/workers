import {
    apiURL_auth,
    apiURL_user,
    apiURL_workers,
    apiURL_workers_cuil,
    apiURL_search,
    apiURL_companies_me,
    apiURL_user_me,
    apiURL_user_profile_image,
    apiURL_password_request,
    apiURL_location_states,
    apiURL_location_cities,
    apiURL_get_service_areas,
    apiURL_services,
    apiURL_categories,
    apiURL_qualifications, apiURL_worker_profile_image
}
    from "../models/api-urls";

import unregister from './apiService.config'; // No estamos usando el register(), pero importarlo hace que funcione el interceptor

import AuthService from './../modules/authService'

const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};

class APIService {

    login = async(email, password) => {
        const response = await fetch(apiURL_auth, {
            method: 'POST',
            headers,
            body: JSON.stringify({
                email,
                password,
            })
        });
        const body = await response.json();

        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    passwordRequest = async(email) => {
        const response = await fetch(apiURL_password_request, {
            method: 'POST',
            headers,
            body: JSON.stringify({
                email,
            })
        });
        const body = await response.json();

        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    registerUser = async( /*{Obj}*/ data) => {
        const response = await fetch(apiURL_user, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        });
        const body = await response.json();

        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    updateUser = async( /*{Obj}*/ data) => {
        const response = await fetch(apiURL_user + data.id, {
            method: 'PUT',
            headers,
            body: JSON.stringify(data)
        });
        const body = await response.json();

        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getWorkerById = async(workerId, end_work) => {
        let url = apiURL_workers + workerId;

        if(end_work) {
            url += '?end_work=' + end_work;
        }
        const response = await fetch(url, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    postWorker = async( /*{Obj}*/ data) => {
        const response = await fetch(apiURL_workers, {
            method: 'POST',
            headers,
            body: JSON.stringify(data)
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getStates = async() => {
        const response = await fetch(apiURL_location_states, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getStateById = async(state_id) => {
        const response = await fetch(apiURL_location_states + state_id, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getCityById = async(city_id) => {
        const response = await fetch(apiURL_location_cities + city_id, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getCityByStateId = async(state_id) => {
        const response = await fetch(apiURL_location_cities + '?state_id=' + state_id, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    searchWorkers = async(params = {}) => {
        let url = new URL(apiURL_search);
        Object.keys(params).forEach(key => {
           url.searchParams.append(key, params[key]);
        });
        console.log(url);
        const response = await fetch(url, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getQualificationsByBooleanEndWork = async(/* [0,1] */end_work) => {
        const response = await fetch(apiURL_qualifications + '?end_work=' + end_work, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getWorkerByCuil = async(/* string */cuil) => {
        const response = await fetch(apiURL_workers_cuil + cuil, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    deleteQualification = async(/* { "id" : qualification_id } */data) => {
        const response = await fetch(apiURL_qualifications, {
            method: 'DELETE',
            headers,
            body: JSON.stringify(data)
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getUserMe = async() => {
        const response = await fetch(apiURL_user_me, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getWorkersAreas = async() => {
        const response = await fetch(apiURL_get_service_areas, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getServices = async() => {
        const response = await fetch(apiURL_services, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getCategories = async() => {
        const response = await fetch(apiURL_categories, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    getMyCompanies = async() => {
        const response = await fetch(apiURL_companies_me, {
            method: 'GET',
            headers
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    postProfileImage = async(/*file data - not string*/ file) => {
        const formData = new FormData();
        formData.append('profile_image',file);

        let url = apiURL_user_profile_image;

        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    postWorkerProfileImage = async(/*file data - not string*/ file, /* string */ workerId) => {
        const formData = new FormData();
        formData.append('profile_image',file);

        let url = apiURL_worker_profile_image;

        if(workerId) {
            url += workerId;
        }

        const response = await fetch(url, {
            method: 'POST',
            headers: {
                'content-type': 'multipart/form-data'
            },
            body: formData
        });
        const body = await response.json();

        if (response.status >= 401) {
            this.callAuthServiceLogout();
            return false;
        }
        if (response.status >= 300) throw Error(body.message);

        return body;
    };

    callAuthServiceLogout() {
        const apiAuthService = new AuthService();
        apiAuthService.logout({redirect: true});
    }

}

//unregister();

export default APIService;