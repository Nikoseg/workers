import React, { Component } from 'react';

import $ from 'jquery'; // TODO: remove jQuery dependency // A warning in console says that we dont use it, but we do
import ReactBootstrapSlider from 'react-bootstrap-slider';
import './../../../node_modules/react-bootstrap-slider/src/css/bootstrap-slider.min.css'

class ProgressBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            slider: {
                step: 1,
                min: 1,
                max: 100
            }
        }
    }

    onSlideStop(obj) {
        this.props.onChangeCallback(this.props.id, this.props.type, obj.target.value);
    }

    calculateRange(max, min, slider_value) {
        const range = max - min;
        return (slider_value * 100) / range;
    }

    render() {
        if(this.props.skills){
            let render = [];
                this.props.skills.forEach(skill => {

                    if(skill.type !== 'slider') { return; }

                    const style = {width: ''};
                    style.width = this.calculateRange(skill.max, skill.min, skill.slider_value) + '%';

                    render.push(<div key={Number(skill.worker_id ? skill.worker_id : skill.id) + Math.random()} className="workers-skill-bar">
                        {skill.title ? <label>{skill.title}</label> : '' }
                        <div className="progress mb-2">
                            <div className="progress-bar" style={style} role="progressbar" aria-valuenow="0" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>);
                });
            return render;
        } else {
            const style = {width: ''};
            style.width = '50%';

            return (<div className="workers-skill-bar m-3 mb-5">
                <label className="p-3 workers-form">{this.props.description}</label>
                <div className="row align-items-center">
                    <div className="col-2">
                        <div className="text-center">{this.props.min}</div>
                        <div className="clearfix"></div>
                        <div className="text-center">{this.props.min_title}</div>
                    </div>
                    <div className="col">
                        <ReactBootstrapSlider
                            id={this.props.id}
                            type={this.props.type}
                            value={this.props.currentValue}
                            slideStop={this.onSlideStop.bind(this)}
                            step={this.state.slider.step}
                            max={this.props.max}
                            min={this.props.min}
                            tooltip={this.props.editionMode ? 'always' : 'show'}
                        />
                    </div>
                    <div className="col-2">
                        <div className="text-center">{this.props.max}</div>
                        <div className="clearfix"></div>
                        <div className="text-center">{this.props.max_title}</div>
                    </div>
                </div>
            </div>);
        }
    }
}

export default ProgressBar;