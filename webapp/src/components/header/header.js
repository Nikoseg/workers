import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'
import Logo from './../../static/img/logo.png'

import { headerNavId, setNavLinksEvent } from './../../models/constants';

import AuthService from './../../modules/authService'
import ProfilePicture from './../../static/img/profile_picture.png';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: false,
            collapsed: true,
            collapsedOverlay: true
        };
        this.auth = new AuthService();
    }

    componentDidMount() {
        // When the component is mounted, add your DOM listener to the "nv" elem.
        // (The "nv" elem is assigned in the render function.)
        this.nav.addEventListener(setNavLinksEvent, () => {
            this.checkUserStatus();
        });
        this.checkUserStatus();
    }

    componentWillUnmount() {
        // Make sure to remove the DOM listener when the component is unmounted.
        this.nav.removeEventListener(setNavLinksEvent, () => {
            this.checkUserStatus();
        });
    }

    checkUserStatus() {
        this.auth.checkLoginStatusAndDoSomethingOrDefault(this.handleLoginHeader, this.handleLogoutHeader);
    }

    // Use a class arrow function (ES7) for the handler. In ES6 you could bind()
    // a handler in the constructor.
    handleLoginHeader = (event) => {
        this.setState({isLoggedIn: true});
    };

    handleLogoutHeader = (event) => {
        this.setState({isLoggedIn: false});
    };

    setCollapseMenuStatusOrToggle(status) {
        const collapsed = status || !this.state.collapsed;
        this.setState({
            collapsed,
            collapsedOverlay : collapsed
        });
    }

    render() {
        const collapsedClassname = 'collapse navbar-collapse';
        const notCollapsedClassname = 'navbar-collapse show';
        const collapsedOverlayClassname = 'menu-overlay-open-detection';
        const notCollapsedOverlayClassname = 'menu-overlay-open-detection show';
        return (
            <nav id={headerNavId} ref={elem => this.nav = elem} className="navbar navbar-expand-lg navbar-light bg-light fixed-top"
                 style={{zIndex: 100}}>
                <div className="container">
                    <a className="navbar-brand" href={
                        // Alta hardcodeada. Revisar TODO
                        this.state.isLoggedIn ? '/my-account'
                        : 'https://workers.com.ar'
                    }><img alt="Workers" srcSet={Logo} width="240" /></a>
                    { this.state.isLoggedIn ?
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarResponsive" aria-controls="navbarResponsive"
                            onClick={()=> this.setCollapseMenuStatusOrToggle()}
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button> :
                        null }
                    <div className={this.state.collapsed ? collapsedClassname : notCollapsedClassname} id="navbarResponsive">
                            {/* !this.state.isLoggedIn ?
                                <ul className="navbar-nav ml-auto">
                                    <li className="nav-item">
                                        <NavLink exact to='/login' className="nav-link">Iniciar Sesión</NavLink>
                                    </li>
                                </ul>
                                : null */}
                            { this.state.isLoggedIn ?
                                <ul className="navbar-nav ml-auto">
                                    <li className="nav-item" onClick={() => this.setCollapseMenuStatusOrToggle(false)}>
                                        <NavLink exact to="/my-factory" className="nav-link">Mi empresa</NavLink>
                                    </li>
                                    <li className="nav-item" onClick={() => this.setCollapseMenuStatusOrToggle(false)}>
                                        <NavLink exact to="/my-account" className="nav-link">Mis empleados</NavLink>
                                    </li>
                                    <li className="nav-item" onClick={() => this.setCollapseMenuStatusOrToggle(false)}>
                                        <NavLink exact to="/qualify" className="nav-link">Calificar</NavLink>
                                    </li>
                                    <li className="nav-item" onClick={() => this.setCollapseMenuStatusOrToggle(false)}>
                                        <NavLink exact to="/search" className="nav-link">Buscar</NavLink>
                                    </li>
                                    <li className="nav-item" onClick={() => this.setCollapseMenuStatusOrToggle(false)}>
                                        <div className="workers-profile-image ml-2" style={
                                            {
                                                backgroundImage: `url('${this.props.profileImage ? this.props.profileImage : ProfilePicture}')`,
                                                width: '36px',
                                                height: '36px',
                                            }
                                        }></div>
                                    </li>
                                </ul>
                                : null }
                    </div>
                </div>
                <div className={this.state.collapsedOverlay ? collapsedOverlayClassname : notCollapsedOverlayClassname} onClick={() => this.setCollapseMenuStatusOrToggle(false)}></div>
            </nav>
        );
    }
}

export default Header;