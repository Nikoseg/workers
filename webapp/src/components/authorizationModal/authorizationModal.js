import React, { Component } from 'react';
import APIService from "../../modules/apiService";
import Logo from './../../static/img/logo-2.png'

class AuthorizationModal extends Component {

    constructor(props){
        super(props);

        this.state = {
            cuit: null,
            razon_social: null,
            fantasy_name: null
        };

        this.api = new APIService();
    }

    componentDidMount() {
        this.api.getUserMe()
            .then((res)=>{
                let mergedObj = Object.assign({}, this.state, res.data);
                this.setState(mergedObj);

                this.api.getMyCompanies().then((res)=>{
                    let mergedObj = Object.assign({}, this.state, res.data);
                    this.setState(mergedObj);

                    if(this.props.callback) {
                        this.props.callback();
                    }

                })
            }).catch((err)=>{
                throw new Error(err);
            //this.showModal('Error', err.message);
        });
    }

    render() {
        const controlsClassname = 'btn btn-large btn-lg btn-primary';

        let sectionClassname = this.props.controls
            ? 'container workers-auth-text workers-scroll-content__controls-scroll' : 'container workers-auth-text';

        return (
            <section className={sectionClassname}>
                <div className="text-center m-5"><img alt="Workers" srcSet={Logo} width="200" /></div>
                <h1 className="text-center mb-5">AUTORIZACION PARA CALIFICAR Y DIVULGAR INFORMACION</h1>
                <ul className="mb-5 pb-5">
                    <li>POR MEDIO DE LA PRESENTE DECLARO EN FORMA VOLUNTARIA QUE AUTORIZO A LA FIRMA {this.state.razon_social ? this.state.razon_social : this.state.fantasy_name || '…………………………………………………………………………………………….………'} CON NUMERO DE CUIT {this.state.cuit || '…………-……………...……..…………-………'} A QUE SUMINISTRE Y DIVULGUE INFORMACION RELACIONADA A MIS DATOS PERSONALES, INFORMACION ACADEMICA, ANTECEDENTES, CALIFICACIONES Y OBSERVACIONES SOBRE MI DESEMPEÑO REAL DURANTE EL COMPLETO PERIODO DE TIEMPO QUE DURASE EL VINCULO Y HASTA 30 DIAS POSTERIORES AL CESE LABORAL, MI SITUACION LEGAL LABORAL Y SINDICAL ACTUAL Y OTRA INFORMACION RELACIONADA A MI CAPACIDAD PARA DESEMPEÑARME EN CUALQUIER ACTIVIDAD, A TRAVES DE LA PLATAFORMA WORKERS.COM.AR</li>
                    <li>EN TAL SENTIDO SE AUTORIZA A OBTENER TODA LA INFORMACION NECESARIA FALTANTE O NO PROVISTA POR MI PERSONA POR LOS DISTINTOS MEDIOS O FUENTES, PUBLICAS O PRIVADAS Y A PUBLICARLAS O DIFUNDIRLAS CON OTROS USUARIOS DE LA PLATAFORMA WORKERS.COM.AR EN CARÁCTER DE USUARIOS TIPO “EMPRESA”. RENUNCIANDO A RECLAMAR LA MUESTRA, ELIMINACION, EDICION Y/O PRESENTACION DE DICHA INFORMACION COMO PERSONA FISICA Y/O USUARIO DEL TIPO “EMPLEADO”</li>
                    <li>AUTORIZO A PERSONAL DE LA EMPRESA A GENERAR Y CARGAR LAS CALIFICACIONES Y/O  PUNTUACIONES Y OBSERVACIONES CORRESPONDIENTES A MI DESEMPEÑO INDIVIDUAL Y EN EQUIPO SIN RESTRICCIONES NI LIMITACIONES, ENTENDIENDO QUE SE HARA DE BUENA FE Y RESPETANDO EN SU TOTALIDAD LO EXPUESTO COMO BASES Y CONDICIONES DE LA PLATAFORMA WORKERS.COM.AR.</li>
                    <li>AUTORIZO A LA EMPRESA Y A LA PLATAFORMA WORKERS.COM.AR A CONSERVAR Y HACER USO DENTRO DE LA PLATAFORMA WORKERS.COM.AR DE TODA LA INFORMACION OBTENIDA FORMANDO PARTE DE MI LEGAJO Y DE LA BASE DE DATOS.</li>
                    <li>POR TODO LO MENCIONADO ANTERIORMENTE, RENUNCIO A CUALQUIER RECLAMO GENERAL, DE DAÑO O PERJUICIO PERSONAL O LABORAL QUE DICHAS CALIFICACIONES, PUNTUACIONES Y/U OBSERVACIONES PUDIERAN GENERARME AHORA O EN UN FUTURO.</li>
                </ul>

                <div className="mt-5 pt-5">
                    <div className="mb-3">*FIRMA DEL AUTORIZANTE:         ______________________________________</div>
                    <div className="mb-3">*ACLARACION:                               ______________________________________</div>
                    <div className="mb-3">*DNI O CUIT:                                  ______________________________________</div>
                    <div className="mb-3">*LUGAR Y FECHA:                         ______________________________________</div>
                    <div className="mb-3">NUMERO DE LEGAJO:                  ______________________________________</div>
                </div>
                <div className="row mt-5 p-3 d-print-none text-center">
                    <div className="col">
                        <a className={controlsClassname}
                           href='/authDownload/AUTORIZACION PARA CALIFICAR Y DIVULGAR INFORMACION.docx'
                           target='_blank'>Descargar documento</a>
                    </div>
                </div>
            </section>
        );
    }
}

export default AuthorizationModal;