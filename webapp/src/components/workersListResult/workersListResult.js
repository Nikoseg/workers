import React, {Component} from 'react';
import ProfilePicture from './../../static/img/profile_picture.png';
import ProgressBar from './../../components/progressBar/progressBar';
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class WorkersListResult extends Component {

    getWorkerTags(worker){
        const render = [];
        if(worker.services){
            worker.services.forEach((service)=>{
                render.push(<div key={worker.id + service.id} className="label btn btn-primary m-2">{service.name}</div>);
            });
        }
        return render;
    }

    render() {
        const workersItems = this.props.workers.map((worker, index) => {
            return <a href={'/worker-profile/' + worker.id} target="_blank" className="workers-list-results p-3" key={index+worker.id} value={worker.id}>
                <div className="row mb-5">
                    <div className="col-sm-12 col-md-2 mb-4 mb-md-0">
                        <div className="workers-profile-image" style={
                            {
                                backgroundImage: `url('${worker.full_profile_image ? worker.full_profile_image : ProfilePicture}')`,
                                maxWidth: '80px',
                                maxHeight: '80px',
                                backgroundSize: 'cover',
                                width: '100%'
                            }
                        }></div>
                    </div>
                    <div className="col text-center text-md-left">
                        <h2>{worker.name} {worker.lastname}</h2>
                        <h3>{worker.city && worker.city.name ? worker.city.name + ',' : ''} {worker.state && worker.state.name ? worker.state.name : ''}</h3>
                        <h4><i className="fa fa-star"></i> {worker.scoring}</h4>
                        <div>{this.getWorkerTags(worker)}</div>
                    </div>
                    <div className="col d-none d-md-block">
                        <ProgressBar skills={worker.qualifications} editionMode={false}/>
                    </div>
                </div>
            </a>});

        return (
            <div>
                {workersItems}
            </div>
        );
    }
};

const mapDispatchToProps = dispatch => bindActionCreators({
    goToWorkerProfile: (id) => push('/worker-profile/' + id)
}, dispatch)

export default connect(
    null,
    mapDispatchToProps
)(WorkersListResult)