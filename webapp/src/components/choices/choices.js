import React, { Component } from 'react';

class Choices extends Component {

    // Option type selection logic...
    selectChoice(obj) {

        if(!this.props.editionMode) {
            return;
        }

        const target= obj.target;

        const parentNode = target.parentNode;
        const children = parentNode.children;
        // Clean all css classess
        for (let i = 0; i < children.length; i++) {
            children[i].classList = this.props.unselectedButtonCss;
            // Do stuff
        }

        // Apply CSS to option selection
        target.classList = this.props.selectedButtonCss;

        // To send
        if(this.props.onChangeCallback) {
            this.props.onChangeCallback(this.props.id, this.props.type, target.innerText);
        }
    }

    render() {
        const options = this.props.options.map((option) => {

            let className = this.props.unselectedButtonCss;

            if(this.props.selectedChoice && this.props.selectedChoice === option) {
                className = this.props.selectedButtonCss;
            }

            return <div key={option} className={className} onClick={(option) => {this.selectChoice(option)}}>{option}</div>
        });
        const render = <section key={this.props.title}>
            <div className="row justify-content-center">
                <div className="col">
                    <label className="workers-form">{this.props.title}</label>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col">
                    <div className="form-group">
                        {options}
                    </div>
                </div>
            </div>
        </section>;
        return render;
    }
}

export default Choices;