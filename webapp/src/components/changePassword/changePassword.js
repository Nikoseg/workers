import React, { Component } from 'react';

import AuthService from './../../modules/authService'

import FullscreenLoading from './../../components/fullscreenLoading/fullscreenLoading'

import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';

class ChangePassword extends Component {

    constructor(props) {
        super(props);

        this.state = {
            oldPassword: '',
            newPassword: '',
            newPasswordRepeated: '',
            fullscreenLoadingVisible: false,
            fullscreenLoadingMessage: 'Cargando...',
            visible: false,
            modalTitle: '',
            modalContent: ''
        };
        this.auth = new AuthService();
    }

    handleChange(event) {
        this.setState({[event.target.id]: event.target.value});
    }

    send(event){
        event.preventDefault();
        /*this.setState({
            fullscreenLoadingVisible: true
        });*/
        this.auth.changePassword(this.state).then(() => {
            /*this.setState({
                fullscreenLoadingVisible: false
            });*/
            this.showModal('Éxito', 'La contraseña se ha cambiado con éxito');
        }).catch((err) => {
            /*this.setState({
                fullscreenLoadingVisible: false
            });*/
            this.showModal('Error', err && err.message ? err.message : 'Ocurrió un error');
        });
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({
            visible: false,
        });
    }
    //

    render() {
        return (
            <form onSubmit={(event)=> this.send(event)}>
                <div className="row justify-content-center">
                    <div className="col-12">
                        <div className="form-group">
                            <label>Ingrese su anterior contraseña</label>
                            <input className="form-control"
                                   type="password"
                                   placeholder="Contraseña anterior"
                                   required="required"
                                   id="oldPassword"
                                   value={this.state.oldPassword} onChange={this.handleChange.bind(this)}
                            />
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-12">
                        <div className="form-group">
                            <label>Ingrese su nueva contraseña</label>
                            <input className="form-control"
                                   type="password"
                                   placeholder="Nueva contraseña"
                                   required="required"
                                   id="newPassword"
                                   value={this.state.newPassword} onChange={this.handleChange.bind(this)}
                            />
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-12">
                        <div className="form-group">
                            <label>Repita su nueva contraseña</label>
                            <input className="form-control"
                                   type="password"
                                   placeholder="Repetir nueva contraseña"
                                   required="required"
                                   id="newPasswordRepeated"
                                   value={this.state.newPasswordRepeated} onChange={this.handleChange.bind(this)}
                            />
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-12">
                        <div className="form-group text-center">
                            <button type="submit" className="btn btn-lg btn-primary">Enviar</button>
                        </div>
                    </div>
                </div>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
                <FullscreenLoading visible={this.state.fullscreenLoadingVisible} text={this.state.fullscreenLoadingMessage} />
            </form>
        );
    }
}

export default ChangePassword;