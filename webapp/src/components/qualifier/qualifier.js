import React, { Component } from 'react';
import ProgressBar from './../progressBar/progressBar'
import Textarea from './../textarea/textarea'
import Choices from './../choices/choices'

import APIService from "../../modules/apiService";

import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';


class Qualifier extends Component {

    constructor(props) {
        super(props);

        this.state = {
            qualifications: false,
            selectedButtonCss: 'btn btn-primary m-1',
            unselectedButtonCss: 'btn btn-outline-primary m-1'
        };

        this.selectedQualificationOption = 0; // WARNING: Local copy de this.props.endWork
        this.api = new APIService();

        this.qualifications = {
            0: [],
            1: [],
        };

        this.shouldComponentUpdateFlag = true;
    }

    componentDidMount() {
        this.selectedQualificationOption = this.props.endWork;
        this.setOption(this.props.endWork);

        if (this.props.editionMode) {
            this.initialLoad(this.selectedQualificationOption);
        }
    }

    // shouldComponentUpdate() {
    //     return this.shouldComponentUpdateFlag;
    // }

    componentDidUpdate(prevProps) {
        const { endWork: currentEndWork } = this.props;
        const { endWork: prevEndWork } = prevProps;

        if (prevEndWork !== currentEndWork) {
            this.setOption(currentEndWork);
        }
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    setOption(/*string*/option) {
        this.shouldComponentUpdateFlag = true;
        this.setState({
            qualifications: this.qualifications[option]
        });
        this.selectedQualificationOption = option;
        this.props.onEndWorkChange(this.selectedQualificationOption); // Parent advice of this change
        setTimeout(()=>{
            this.shouldComponentUpdateFlag = false;
            this.props.resetQualificationArray();
            this.sendQualificationsStateToParent();
        }, 0); // @TODO: Review later this ugly ugly solution
    }

    initialLoad(/*string*/option) {

        this.api.getQualificationsByBooleanEndWork(0).then((res) => {
            this.qualifications[0] = res.data;
            this.api.getQualificationsByBooleanEndWork(1).then((res) => {
                this.qualifications[1] = res.data;
                this.setState({
                    qualifications: this.qualifications[option]
                });
                this.selectedQualificationOption = option;
                this.shouldComponentUpdateFlag = false;

                this.sendQualificationsStateToParent();
            })
        }).catch((err) => {
            this.showModal('Ha ocurrido un error', err.message);
        });
    }

    onChangeCallback(id, type, value) {
        const qualificationObj = {
            id,
            type,
            value
        };
        this.props.onQualificationsChange(qualificationObj);
    }

    sendQualificationsStateToParent(){
        const qualificationsObj = this.state.qualifications;
        if(!qualificationsObj){
            return;
        }
        qualificationsObj.map((q) => {
            if(q.type === 'slider') {
                q.value = q.max / 2
            }
        });

        qualificationsObj.forEach((q) => {
            const qualificationObj = {
                id: q.id,
                type: q.type,
                value: q.value
            };
            this.props.onQualificationsChange(qualificationObj);
        });

        this.setState({
            qualifications: qualificationsObj
        });
    }

    render() {
        let qualifications = this.state.qualifications;
        let qualificationsDom;
        if (qualifications) {
            qualificationsDom = qualifications.map((qualification) => {
                    let render;
                    switch (qualification.type) {
                        case 'slider': // SLIDER
                            render = <ProgressBar
                                id={qualification.id}
                                type={qualification.type}
                                key={qualification.id}
                                description={qualification.description}
                                min={Number(qualification.min)}
                                min_title={qualification.min_title}
                                max={Number(qualification.max)}
                                max_title={qualification.max_title}
                                editionMode={qualification.editionMode}
                                currentValue={Number(qualification.max / 2)}
                                onChangeCallback={this.onChangeCallback.bind(this)}
                            />;
                            break;
                        case 'textarea': // TEXTAREA
                            render = <Textarea
                                id={qualification.id}
                                type={qualification.type}
                                key={qualification.title}
                                title={qualification.title}
                                placeholder={qualification.placeholder}
                                editionMode={this.props.editionMode}
                                onChangeCallback={this.onChangeCallback.bind(this)}
                            />;
                            break;
                        case 'choises': // CHOICES
                            render = <Choices
                                id={qualification.id}
                                type={qualification.type}
                                key={qualification.title}
                                title={qualification.title}
                                options={qualification.options}
                                selectedButtonCss={this.state.selectedButtonCss}
                                unselectedButtonCss={this.state.unselectedButtonCss}
                                editionMode={this.props.editionMode}
                                onChangeCallback={this.onChangeCallback.bind(this)}
                            />;
                            break;
                        default:
                            render = <div key={qualification.title}>{qualification.title}</div>;
                    }
                    return render; // Map return
                }
            );
        }

        return (
            <section className="workers-qualifier">
                {/* We need to review how we want to implement this section
                in qualify component */}
                {/* {this.props.editionMode ?
                <div className="row">
                    <div className="col workers-qualify-profile-data">
                        <h1>Está activo laboralmente</h1>
                        <div>
                            <div
                                className={this.selectedQualificationOption === 1 ? this.state.selectedButtonCss : this.state.unselectedButtonCss}
                                onClick={() => this.setOption(1)}>Sí
                            </div>
                            <div
                                className={this.selectedQualificationOption === 0 ? this.state.selectedButtonCss : this.state.unselectedButtonCss}
                                onClick={() => this.setOption(0)}>No
                            </div>
                        </div>
                    </div>
                </div>
                    : null} */}
                {qualificationsDom}
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                       className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
            </section>
        );
    }
}

export default Qualifier;
