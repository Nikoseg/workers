import React, { Component } from 'react';

class FullscreenLoading extends Component {

    render() {
        let className = this.props.visible ? 'fullscreen-loading show' : 'fullscreen-loading';
        return (
           <div className={className}>
               <div className="row justify-content-center noselect">
                   <div className="col text-center align-self-center">{this.props.text || 'Cargando...'}</div>
               </div>
           </div>
        );
    }
}

export default FullscreenLoading;