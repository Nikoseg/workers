import React, { Component } from 'react';

class Textarea extends Component {

    onBlurTextarea(event) {
        this.props.onChangeCallback(this.props.id, this.props.type, event.target.value);
    }


    render() {
        return (<section key={this.props.title}>
            <div className="row justify-content-center">
                <div className="col">
                    <label className="workers-form">{this.props.title}</label>
                </div>
            </div>
            <div className="row justify-content-center">
                <div className="col">
                    <div className="form-group">
                        <textarea className="form-control workers-textarea p-3" id="message" name="message"
                                  onBlur={(e) => {this.onBlurTextarea(e)}}
                                  placeholder={this.props.placeholder} rows="5"></textarea>
                    </div>
                </div>
            </div>
        </section>
        );
    }
}

export default Textarea;