import React, { Component } from 'react';

import AsyncSelect from 'react-select/lib/Async';
import chroma from 'chroma-js';
//import { colourOptions } from '../data';

const workersColor = '#23d8d3'; // TODO: revisar despues si está bien que esto esté acá
const workersMultiSelectClassname = 'workers-multi-select'; // TODO: revisar despues si está bien que esto esté acá

const colourOptions = [
    { value: 'ocean', label: 'Ocean', color: '#00B8D9', isFixed: true },
    { value: 'blue', label: 'Blue', color: '#0052CC', disabled: true },
    { value: 'purple', label: 'Purple', color: '#5243AA' },
    { value: 'red', label: 'Red', color: '#FF5630', isFixed: true },
];

const colourStyles = {
    control: styles => ({ ...styles, backgroundColor: 'white' }),
    multiValue: (styles, { data }) => {
        //const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: workersColor,
            textTransform: 'uppercase'
        };
    },
    multiValueLabel: (styles, { data }) => ({
        ...styles,
        color: 'white',
    }),
    multiValueRemove: (styles, { data }) => {
        const color = chroma(workersColor);
        return {
            ...styles,
            color: 'white',
            ':hover': {
                backgroundColor: color.alpha(0.1).css(),
                color: 'white',
            },
        };
    },
};

const filterColors = (inputValue) =>
    colourOptions.filter(i =>
        i.label.toLowerCase().includes(inputValue.toLowerCase())
    );

const promiseOptions = inputValue =>
    new Promise(resolve => {
        setTimeout(() => {
            resolve(filterColors(inputValue));
        }, 1000);
    });

const getOptions = () => {
    return colourOptions;
};

export default class AsyncMulti extends Component {
    state = { inputValue: '' };
    handleInputChange = (newValue) => {
        const inputValue = newValue.replace(/\W/g, '');
        this.setState({ inputValue });
        return inputValue;
    };

    render() {
        return (
            <AsyncSelect
                isMulti
                cacheOptions
                defaultOptions
                //options={getOptions}
                loadOptions={promiseOptions}
                className={workersMultiSelectClassname}
                styles={colourStyles}
                placeholder='Selecciona...'
                loadingMessage={()=> 'Buscando...'}
                noOptionsMessage={()=> 'No se encontraron resultados...'}
            />
        );
    }
}
