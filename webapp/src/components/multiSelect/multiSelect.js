import React, {Component} from 'react';

import Select from 'react-select';
import chroma from "chroma-js";

import APIService from "../../modules/apiService";

const workersColor = '#23d8d3'; // TODO: revisar despues si está bien que esto esté acá

const colourStyles = {
    control: styles => ({
        ...styles,
        backgroundColor: 'white',
        border: '2px solid #ccc',
        height: '40px',
    }),
    multiValue: (styles, { data }) => {
        //const color = chroma(data.color);
        return {
            ...styles,
            backgroundColor: workersColor,
            textTransform: 'uppercase'
        };
    },
    multiValueLabel: (styles, { data }) => ({
        ...styles,
        color: 'white',
    }),
    multiValueRemove: (styles, { data }) => {
        const color = chroma(workersColor);
        return {
            ...styles,
            color: 'white',
            ':hover': {
                backgroundColor: color.alpha(0.1).css(),
                color: 'white',
            },
        };
    },
};

export default class MultiSelect extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            defaultValue: null,
            // other states
        };

        this.defaultValue = [];

        this.options = [];

        this.workerId;

        this.api = new APIService();
    }

    getOptions = (apiServiceFunction) => {
        apiServiceFunction().then((res) => {
            this.options = res.data;
            this.options.map((option) => {
               option.value = option.id;
               option.label = option.name
            });

            if(this.workerId && this.props.preset){
                this.defaultValue = [];
                this.api.getWorkerById(this.workerId).then((res) => {
                  if(res && res.data) {
                      if(res.data[this.props.preset].length > 0){
                          res.data[this.props.preset].forEach((preset) => {
                              this.defaultValue.push(preset);
                          });
                          this.defaultValue.map((option) => {
                              option.value = option.id;
                              option.label = option.name
                          });

                          this.setState({
                              isLoading: false
                          });
                      } else {
                          this.setState({
                              isLoading: false
                          });
                      }
                  }
                }).catch((err) => {
                    //this.showModal('No se pudieron precargar algunas opciones', err.message);
                    console.log('No se pudieron precargar algunas opciones:', err.message);
                });
            } else {
                this.setState({
                    isLoading: false
                });
            }
        });
    };

    refreshComponent(workerId) {
        this.workerId = workerId;
        this.setState({
            isLoading: true
        });
        this.getOptions(this.props.endpoint);
    }

    componentDidMount() {
        this.getOptions(this.props.endpoint);
    }

    render() {
        if(this.state.isLoading){
            return null;
        } else {
            //console.log(this.defaultValue);
            return (<Select
                id="pepe"
                isMulti
                defaultValue={this.defaultValue}
                name="colors"
                options={this.options}
                className="basic-multi-select"
                classNamePrefix="select"
                styles={colourStyles}
                onChange={this.props.onMultiSelectChange}
            />)
        }
    }
};