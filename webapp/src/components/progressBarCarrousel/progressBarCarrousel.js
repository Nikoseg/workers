import React, { Component } from 'react';
import Carousel from '@brainhubeu/react-carousel';
import './../../../node_modules/@brainhubeu/react-carousel/lib/style.css';

import ProgressBar from './../progressBar/progressBar';

import Rodal from 'rodal';
// TODO: Revisar...
import 'rodal/lib/rodal.css';

import APIService from './../../modules/apiService';

import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


class ProgressBarCarrousel extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0
        };

        this.api = new APIService();

        this.qualificationIdToDelete = 0;
    }

    /* MODAL Functions */
    showModal(title, content, showButtons) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.setState({ showButtons });
        this.show();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    // we check if we got event from input (and it has target property) or just value from Carousel
    onChange = e => this.setState({ value: e.target ? e.target.value : e });

    deleteQualification(qualificationId){
        this.qualificationIdToDelete = qualificationId;
        this.showModal('¿Está seguro de querer eliminar esta calificación?', null, true);
    }

    confirmDeleteQualification() {
        const objData = {
            "id": this.qualificationIdToDelete
        };
        this.api.deleteQualification(objData).then((res)=>{
            this.hide();
            //this.showModal('Éxito', res.message, false)
        }).catch((err) => {
            this.showModal('Error', err.message, false);
        });

        if(this.props.updateView){
            //setTimeout(() => this.props.updateView(),1500);
            this.props.updateView();
        }
    }

    setRender(q) {
        let date = new Date(q.created_at) || null;
        let workerId = q.worker_id;
        let qualificationId = q.id;
        return (
            <section className="container pt-0" key={qualificationId}>
                <div className="row">
                    <div className="col-12 pl-md-5 pr-md-5 pb-3">
                        <p>Calificación del: <span className="text-dark">{date.toLocaleDateString()}</span></p>
                        <ProgressBar skills={q.answers} editionMode={false}/>
                    </div>
                </div>
                <div className="row mt-md-3 pl-md-4">
                    <div className="col text-center text-md-left">
                        <p>
                            <a className="btn btn-lg m-1 m-md-0 btn-primary" href={'/worker-profile/' + workerId} target="_blank">Ver Perfil</a>
                            &nbsp;
                            <button id="worker-carrousel-btn-qualify" className="btn btn-lg m-1 m-md-0 btn-outline-primary" onClick={() => this.props.goToQualify(workerId)}>Recalificar</button>
                            &nbsp;
                            {/*<button id="worker-carrousel-btn-delete" className="btn btn-lg m-1 m-md-0 btn-outline-danger" onClick={() => this.deleteQualification(qualificationId)}>Eliminar</button>*/}
                        </p>
                    </div>
                </div>
            </section>
        );
    }

    render() {
        const qualifications = this.props.qualifications;
        const qualificationsDom = qualifications.map((q) => this.setRender(q));
        const renderArrows = !(qualifications.length < 2);

        return (
            <div>
                {/*<input value={this.state.value} onChange={this.onChange} type="number" />*/}
                <Carousel
                    value={this.state.value}
                    onChange={this.onChange}
                    arrowLeft={renderArrows ? <i className="fa fa-4x cursor-pointer text-black-50 fa-caret-left"></i> : null}
                    arrowRight={renderArrows ? <i className="fa fa-4x cursor-pointer text-black-50 fa-caret-right"></i> : null}
                    addArrowClickHandler = {renderArrows}
                    arrow={renderArrows}
                >
                    {qualificationsDom}
                </Carousel>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                    <h4 className="mt-4 pt-4 mb-4 text-center">{this.state.modalTitle}</h4>
                    <div className="container mt-5 mb-5">
                        <div className="text-center">{this.state.modalContent}</div>
                        { this.state.showButtons ?
                        <div className="row justify-content-center">
                            <div className="col-auto">
                                <button className="btn btn-lg btn-danger" onClick={() => this.confirmDeleteQualification()}>Sí, eliminar</button>
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-lg btn-info" onClick={()=> this.hide()}>Cancelar</button>
                            </div>
                        </div> : null }
                    </div>
                </Rodal>
            </div>
        );
    }
}

//export default ProgressBarCarrousel;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToWorkerProfile: (id) => push('/worker-profile/' + id),
    goToQualify: (id) => push('/qualify/' + id)
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(ProgressBarCarrousel)