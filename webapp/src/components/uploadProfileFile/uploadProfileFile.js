import React, { Component } from 'react';

import Rodal from 'rodal';
// TODO: Revisar...
import 'rodal/lib/rodal.css';

// API SERVICE
import APIService from './../../modules/apiService';

class UploadProfileFile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            modalTitle: '',
            modalContent: '',
            file: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.api = new APIService();
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    handleChange(event) {
        if(this.props.preview){
            this.props.updateListener(/* file */ event.target.files[0]);
            return;
        }
        this.api.postProfileImage(event.target.files[0])
            .then((res)=>{
                //this.showModal('Éxito', res.message);
                if(this.props.updateListener) {
                    this.props.updateListener();
                }
            }).catch((err) => {
                this.showModal('Error', err.message);
        });
    }

    render() {
        return (
            <label className="workers-upload-file">
                <i className="fa fa-camera"></i>
                <input type="file" className="d-none"
                       name="profile_image"
                       value={this.state.profile_image} onChange={this.handleChange} />
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <p className="text-center">{this.state.modalContent}</p>
                </Rodal>
            </label>
        );
    }
}

export default UploadProfileFile;