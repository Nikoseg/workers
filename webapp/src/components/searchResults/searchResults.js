import React, { Component } from 'react';
import WorkersListResult from './../../components/workersListResult/workersListResult';

import Pagination from "react-js-pagination";


class SearchResults extends Component {

    render() {
        return (
            <div className="workers-search-results">
                <label className="label-title">Resultados</label>
                {this.props.results ? <WorkersListResult workers={this.props.results}/> : 'No se encontraron resultados'}
                <div className="row justify-content-center p-5">
                    <nav aria-label="Page navigation">
                        <Pagination
                            activePage={this.props.activePage}
                            itemsCountPerPage={3}
                            totalItemsCount={this.props.paging.total}
                            pageRangeDisplayed={3}
                            itemClass='page-item'
                            linkClass='page-link'
                            onChange={this.props.handlePageChange}
                        />
                    </nav>
                </div>
            </div>
        );
    }
}

export default SearchResults;