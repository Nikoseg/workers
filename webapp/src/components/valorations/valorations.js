import React, { Component } from 'react';

import ProfilePicture from './../../static/img/profile_picture.png';
import ProgressBar from './../../components/progressBar/progressBar';
import Choices from './../../components/choices/choices'

class Valorations extends Component {

    constructor(props) {
        super(props);

        //this.state
    }

    calculateScoring(qualifications) {
        let result = 0;
        const results = [];
        qualifications.forEach((q) => {
           if(q.type === 'slider') {
            const min = Number(q.min);
            const max = Number(q.max);
            const sliderValue = Number(q.slider_value);
            //results.push((sliderValue * 100) / (max-min));
            results.push((sliderValue * 100) / (max));
           }
        });

        results.forEach((res) => {
            result += res;
        });

        return (Math.floor(result / results.length) / 10).toFixed(2);
    }

    getTextAreaValue(qualifications) {
        let result = '';
        qualifications.forEach(q => {
            if(q.type === 'textarea') {
                result = q.text;
            }
        });
        return result;
    }

    getChoicesAnswers(qualifications) {
        const selectedButtonCss = 'label btn btn-primary m-1';
        const unselectedButtonCss = 'label btn btn-outline-primary m-1 disabled-label';

        let result = [];
        qualifications.forEach(q => {
            if(q.type === 'choises') {
                result.push(<div key={'choice_' + q.id} className="row mt-5">
                    <div className="col"><Choices
                    id={q.id}
                    type={q.type}
                    key={q.title}
                    title={q.title}
                    options={Array.isArray(q.options) ? q.options : q.options.split(', ')}
                    selectedButtonCss={selectedButtonCss}
                    unselectedButtonCss={unselectedButtonCss}
                    selectedChoice={q.option_value}
                    editionMode={false}
                    /></div></div>);
            }
        });
        return result;
    }

    render() {
        //const scoring = this.calculateScoring(this.props.answers);

        let textarea = this.getTextAreaValue(this.props.answers);

        let choices = this.getChoicesAnswers(this.props.answers);

        return(
            <section key={this.props.title + this.props.scoring}>
                <div className="row">
                    <div className="col-sd-12 col-md-2 text-center">
                        <div className="workers-profile-image" style={
                            {
                                backgroundImage: `url('${this.props.profileImage ? this.props.profileImage : ProfilePicture}')`,
                                width: '100px',
                                height: '100px',
                            }
                        }>
                            {/* width="90%" */}
                        </div>
                    </div>
                    <div className="col">
                        <div className="row align-items-end">
                            <div className="col"><h1 className="mb-0">{this.props.title}</h1></div>
                            <div className="col text-right"><h4><i className="fa fa-star"></i> {this.props.scoring}</h4></div>
                        </div>
                        {
                            textarea ?
                            <div className="row mt-3 mb-3">
                                <div className="col text-black-50">
                                    {textarea}
                                </div>
                            </div>
                                : null
                        }

                        {choices}

                        <div className="row mt-5">
                            <div className="col">
                                <ProgressBar skills={this.props.answers}/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default Valorations;