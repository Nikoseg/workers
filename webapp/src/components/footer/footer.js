import React from "react";

function Footer() {
  return (
    <div className="workers-footer">
        <span className="text-footer m-5 d-inline-block"><a>Centro de Ayuda</a></span>
        <span className="text-footer m-5 d-inline-block"><a>Términos y Condiciones</a></span>
        <span className="text-footer m-5 d-inline-block"><a>Política de Privacidad</a></span>
        <button className="btn btn-lg btn-primary float-right m-5">Contáctanos</button>
    </div>
  );
}

export default Footer;