import React, { Component } from 'react';

import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// import ProfilePicture from './../../static/img/profile_picture.png';
// import ProgressBar from './../progressBar/progressBar'
import ProgressBarCarrousel from './../progressBarCarrousel/progressBarCarrousel'
import IconConfig from './../../static/img/icon-config.png';

// API SERVICE
import APIService from './../../modules/apiService';

class QualificationListProfile extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            qualificationsDom: null,
            disabled: false
        };

        this.api = new APIService();
    }

    renderQualifications(qualifications) {
        const event = new CustomEvent('resetQualificationListProfileComponent', {detail: this.props.id});
        window.dispatchEvent(event); // Reset all opened
        let render = <div><ProgressBarCarrousel key={Math.random()} qualifications={qualifications} updateView={this.props.updateView}/></div>;
        if(this.state.qualificationsDom) {
            this.setState({
                qualificationsDom: null
            });
        } else {
            this.setState({
                qualificationsDom: render
            });
        }
    }

    resetQualificationListProfileComponent(e) {
        if(e.detail === this.props.id) return;
        if(this.state.qualificationsDom) {
            this.setState({
                qualificationsDom: null
            });
        }
    }

    tagDisabledIfApplies(){
        this.api.getWorkerById(this.props.worker.id, 1).then((res)=>{
            if(res.data.qualifications.length > 0) {
                this.setState({
                    disabled: true
                });
            }
        }).catch((err)=>{
            console.log(err);
        })
    }

    componentDidMount() {
        this.tagDisabledIfApplies();
        window.addEventListener('resetQualificationListProfileComponent', this.resetQualificationListProfileComponent.bind(this));
    }

    componentWillUnmount() {
        window.removeEventListener('resetQualificationListProfileComponent', this.resetQualificationListProfileComponent.bind(this));
    }

    render() {
        const randomId = Math.random().toString();
        let globalSkill = [
            {
                max: 10,
                min: 0,
                slider_value: this.props.worker.scoring || 0,
                type: this.props.worker.scoring ? 'slider' : ''
            }
        ];

        let qualifications = this.props.worker.qualifications;
        const cssClassname = 'workers-qualification-list-profile bg-light rounded p-4 align-items-center';

        return (
            <tr
                className={
                    this.props.index % 2 === 0
                        ? "workers-myaccount__table-value"
                        : "workers-myaccount__table-value--color"
                }
                onClick={() => this.renderQualifications(qualifications)}
            >
                <td className="workers-myaccount__table-value-item">
                    {this.props.worker.scoring || ''}
                </td>
                <td className="workers-myaccount__table-value-item">
                    {this.props.worker.name}
                </td>
                <td className="workers-myaccount__table-value-item">
                    {this.props.worker.lastname}
                </td>
                <td className="workers-myaccount__table-value-item">
                    {this.props.worker.cuil}
                </td>
                <td className="workers-myaccount__table-value-item cursor-pointer">
                    <img width="44" height="44" src={IconConfig} />
                </td>
            </tr>
            // <section className="p-0 mb-3">
            //     <div className={this.state.disabled ? cssClassname + ' disabled' : cssClassname}>
            //             <label htmlFor={randomId} className="row align-items-center cursor-pointer" onClick={() => this.renderQualifications(qualifications)}>
            //                 <div className="col-12 col-md-auto pr-2">
            //                     <div className="workers-profile-image" style={
            //                         {
            //                             backgroundImage: `url('${this.props.worker.full_profile_image || ProfilePicture}')`,
            //                             maxWidth: '100px',
            //                             maxHeight: '100px'
            //                         }
            //                     }>
            //                         {/* width="90%" */}
            //                     </div>
            //                 </div>
            //                 <div className="col-12 col-md-4">
            //                     <h2 className="mb-md-0 text-center text-md-left">{this.props.worker.name} {this.props.worker.lastname}</h2>
            //                 </div>
            //                 <div className="col-12 col-md-4">
            //                     <ProgressBar skills={globalSkill}/>
            //                 </div>
            //                 <div className="col text-center text-md-right">
            //                     <h4>{this.props.worker.scoring ? <i className="fa fa-star"></i> : null } {this.props.worker.scoring || ''}</h4>
            //                 </div>
            //             </label>

            //             <div>{this.state.qualificationsDom}</div>
            //         {this.state.disabled ? <div className="text-center text-md-right"><span className="label btn btn-outline-info m-2">Cese laboral</span></div> : null}

            //     </div>
            // </section>
        );
    }
}

export default QualificationListProfile;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToAuthorizationPrint: () => push('/auth-print'),
    goToLogin: () => push('/login')
}, dispatch);
/*
export default connect(
    null,
    mapDispatchToProps
)(QualificationListProfile)*/