import React, { Component } from 'react';

import './../../modules/authService';

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AuthService from "../../modules/authService";


class Terms extends Component {

    constructor(props) {
        super(props);

        this.auth = new AuthService();
    }

    logout() {
        this.auth.logout(); // clears user data
        this.auth.setHeaderInOrderOfUserStatus();
        this.props.goToLogin();
    }

    render() {
        return (
            <section className="container mb-5 p-lg-5 text-justify">
                <div className="row">
                    <div className="col-lg-12 text-center">
                        <h2 className="section-heading mb-5">TÉRMINOS Y CONDICIONES DE USO</h2>
                    </div>
                </div>
                <div className="row">
                    <div class="col-lg-12">
                        <p>Esta sección establece los Términos y Condiciones (en adelante, las "Condiciones Generales") para el uso de los contenidos y servicios (en adelante, los "Servicios"), del sitio web especificado en la cláusula 12 (en adelante, el "Sitio Web"). El Sitio Web y los Servicios son prestados por la empresa detallada en la cláusula 12 (en adelante, "WORKERS").</p>
                        <p>En caso de no estar de acuerdo con las Condiciones Generales, por favor, abandone el Sitio Web y absténgase de utilizar los Servicios que se ofrecen. La utilización por el usuario del Sitio Web se entenderá como aceptación plena y sin reservas de las Condiciones Generales aquí establecidas.</p>
                        <p>En adelante, los términos “Usted” y “Usuario” serán utilizados para hacer referencia a todas las personas físicas y/o jurídicas que por cualquier razón accedan al Sitio Web.</p>
                        <p>1. ACCESO AL SITIO WEB</p>
                        <p>1.1. Acceso al Sitio Web</p>
                        <p>El acceso y utilización del Sitio Web no exige la previa suscripción o registro del Usuario. Sin embargo, es posible que la utilización de algunos de los Servicios ofrecidos a través del Sitio Web requiera la suscripción o registro del Usuario y/o el pago de un precio.</p>
                        <p>1.2. Utilización del Sitio Web</p>
                        <p>1.2.1. El Usuario se compromete a utilizar el Sitio Web de conformidad con estas Condiciones Generales, las leyes aplicables conforme la cláusula 12 y con la moral y buenas costumbres.</p>
                        <p>1.2.2. El Usuario se obliga a abstenerse de utilizar el Sitio Web con fines o efectos ilícitos, contrarios a lo establecido en las Condiciones Generales, lesivos de los derechos e intereses de terceros, o que de cualquier forma puedan dañar, inutilizar, sobrecargar o deteriorar el Sitio Web o impedir la normal utilización del Sitio Web por parte de los Usuarios.</p>
                        <p>1.3. Contenido del Sitio Web</p>
                        <p>1.3.1 Los contenidos de este Sitio Web, tales como texto, información, gráficos, imágenes, logos, marcas, programas de computación, bases de datos, diseños, arquitectura funcional y cualquier otro material (en adelante, el “Contenido”) están protegidos por las leyes aplicables en cada jurisdicción conforme la cláusula 12 incluyendo, pero sin limitación, las leyes sobre derechos de autor, patentes, marcas, modelos de utilidad, diseños industriales y nombres de dominio.</p>
                        <p>1.3.2. Todo el Contenido es propiedad de WORKERS y/o de cualquier otra sociedad vinculada y/o de sus proveedores de contenido. La compilación, interconexión, operatividad y disposición de los contenidos del Sitio Web es de propiedad exclusiva de WORKERS. El uso, adaptación, reproducción y/o comercialización no autorizada del Contenido puede encontrarse penado por la legislación vigente en cada jurisdicción.</p>
                        <p>1.3.3. Usted no copiará ni adaptará el código de programación desarrollado por, o por cuenta de, WORKERS para generar y operar sus páginas, el cual se encuentra protegido por la legislación aplicable y vigente en cada jurisdicción.</p>
                        <p>1.3.4 Toda la información incorporada por los Usuarios pasara a formar parte de las bases de datos privada de la plataforma WORKERS.COM.AR y la misma se reserva el derecho de utilizar, y/o Vender y/o Ceder pudiendo ser de forma onerosa o no, las bases de datos para los siguientes fines, a modo ejemplificativo y no limitándose a Publicidades, Censos públicos y/o privados, etc además de Búsquedas de personal, Calificaciones, etc. De personal no vinculado en la plataforma al momento de la operación.</p>
                        <p>1.4. Uso permitido del Sitio</p>
                        <p>1.4.1. Reglas generales: Los Usuarios tienen prohibido utilizar el Sitio Web para transmitir, distribuir, almacenar o destruir material violando la normativa vigente, de forma que se infrinjan derechos de terceros</p>
                        <p>1.4.2. Reglas de Seguridad del Sitio Web: Los Usuarios tienen prohibido violar o intentar violar la seguridad del Sitio Web, incluyendo pero no limitándose a: (i) el acceso a datos que no estén destinados a tal usuario o entrar en un servidor o cuenta cuyo acceso no está autorizado al Usuario, (ii) evaluar o probar la vulnerabilidad de un sistema o red, o violar las medidas de seguridad o identificación sin la adecuada autorización, (iii) intentar impedir el Servicio a cualquier Usuario, anfitrión o red, incluyendo, pero sin limitación, mediante el envío de virus al Sitio Web, o mediante saturación o ataques de denegación de Servicio, (iv) enviar correos no pedidos, incluyendo promociones y/o publicidad de productos o servicios, o (v) falsificar cualquier cabecera de paquete TCP/IP o cualquier parte de la información de la cabecera de cualquier correo electrónico o en mensajes de foros de debate.</p>
                        <p>1.4.3. Las violaciones de la seguridad del sistema o de la red constituyen delitos penales y pueden derivar en responsabilidades civiles. WORKERS investigará los casos de violaciones a la seguridad del sistema, pudiendo dirigirse a la autoridad judicial o administrativa competente a los efectos de perseguir a los Usuarios involucrados en tales violaciones.</p>
                        <p>1.5. Usos prohibidos</p>
                        <p>1.5.1. El Sitio Web sólo podrá ser utilizado con fines lícitos, para acceder a información referida a los Servicios disponibles a través del mismo. WORKERS prohíbe específicamente cualquier utilización del Sitio Web para:</p>
                        <p>- Anunciar datos biográficos incompletos, falsos o inexactos.</p>
                        <p>- Registrar más de una cuenta correspondiente a un mismo usuario.</p>
                        <p>- Usar cualquier mecanismo para impedir o intentar impedir el adecuado funcionamiento de este Sitio Web o cualquier actividad que se esté realizando en este Sitio Web.</p>
                        <p>- Revelar o compartir su contraseña con terceras personas, o usar su contraseña para propósitos no autorizados.</p>
                        <p>- El uso o intento de uso de cualquier máquina, software, herramienta, agente u otro mecanismo para navegar o buscar en este Sitio Web que sean distintos a las herramientas de búsqueda provistos por WORKERS en este Sitio Web.</p>
                        <p>- Intentar descifrar, descompilar u obtener el código fuente de cualquier programa de software de este Sitio Web.</p>
                        <p>1.5.2. WORKERS se reserva el derecho de suspender o dar de baja a cualquier Usuario que, a exclusivo criterio de WORKERS, no cumpla con los estándares definidos en estas Condiciones Generales o con las políticas vigentes de WORKERS, sin que ello genere derecho a resarcimiento alguno.</p>
                        <p>1.6. Canales de comunicación disponibles para los Usuarios</p>
                        <p>1.6.1 El Usuario deberá utilizar los canales de comunicación y calificación disponibles — como el buscador y calificador del sistema, e-mails, chats, foros de discusión, entre otros — (en adelante, las “Condiciones Generales”) de forma responsable, correcta y dando fiel cumplimiento a la normativa vigente.</p>
                        <p>1.6.2. El contenido de cada mensaje enviado y/o carga de datos por el Usuario a través de los Canales es de única y exclusiva responsabilidad del Usuario. WORKERS no garantiza la veracidad de los datos personales y/o contenidos de cada mensaje y/o carga de datos  efectuados y/o publicados en los Canales por el Usuario. El Usuario acepta voluntariamente que el acceso a y/o el uso de los Canales tiene lugar, en todo caso, bajo su exclusiva y única responsabilidad.</p>
                        <p>1.6.3. WORKERS se reserva el derecho a suspender temporal o definitivamente de los Canales y/o de los Servicios sin previo aviso, a quien no respete las presentes Condiciones Generales o realice actos que atenten contra el normal funcionamiento de los Servicios y/o de los Canales y/o del Sitio Web.</p>
                        <p>1.6.4. El Usuario reconoce y acepta que las siguientes conductas se encuentran terminantemente prohibidas:</p>
                        <p>a) utilizar lenguaje vulgar /obsceno, discriminatorio y/u ofensivo;</p>
                        <p>b) todo tipo de ataque personal contra Usuarios y/o terceros, mediante acoso, amenazas, insultos;</p>
                        <p>c) todo acto contrario a las leyes, la moral y las buenas costumbres;</p>
                        <p>d) publicar mensajes, imágenes e hipervínculos agraviantes, difamatorios, calumniosos, injuriosos, falsos, discriminatorios, pornográficos, de contenido violento, insultantes, amenazantes, incitantes a conductas ilícitas o peligrosas para la salud, y/o que vulneren de cualquier forma la privacidad de cualquier tercero como así también la violación directa o indirecta de los derechos de propiedad intelectual de WORKERS y/o de cualquier tercero, incluyendo clientes de WORKERS;</p>
                        <p>e) publicar mensajes que puedan herir y/o afectar la sensibilidad del resto de los Usuarios y/o de cualquier tercero, incluyendo clientes de WORKERS;</p>
                        <p>f) promocionar, comercializar, vender, publicar y/u ofrecer cualquier clase de productos, servicios y/o actividades por intermedio de o a través de la utilización de los Canales, excepto aquellas expresamente permitidas por WORKERS;</p>
                        <p>g) la venta, locación o cesión, ya sea a título oneroso o gratuito, del espacio de comunicación de los Canales;</p>
                        <p>h) publicar mensajes que de cualquier forma contengan publicidad;</p>
                        <p>i) el uso o envío de virus informáticos, malware, spyware, ransomware y/o la realización de todo acto que cause o pudiera causar daños o perjuicios al normal funcionamiento de los Servicios y/o los Canales, o de los equipos informáticos o software de WORKERS y/o de cualquier tercero, incluyendo clientes de WORKERS;</p>
                        <p>j) todo acto dirigido a enmascarar y/o falsificar o disimular direcciones IP, correos electrónicos y/o cualquier otro medio técnico de identificación de los Usuarios o sus equipos informáticos;</p>
                        <p>k) todo acto que viole la privacidad de otros Usuarios, o que viole cualquiera de sus derechos bajo la Ley N° 25.326;</p>
                        <p>l) la publicación de datos personales sin el consentimiento expreso del titular de los mismos;</p>
                        <p>m) la transmisión o divulgación de material que viole la legislación en vigor en el país y/o que pudiera herir la sensibilidad del resto de los Usuarios y/o de cualquier tercero, incluyendo clientes de WORKERS;</p>
                        <p>n) la publicación de cualquier tipo de contenido en violación de derechos de terceros, incluyendo sin limitación los derechos de propiedad intelectual y/o industrial.</p>
                        <p>1.6.5. WORKERS no tiene obligación de controlar ni controla la utilización que el Usuario haga de los Canales. No obstante ello, WORKERS se reserva el derecho de no publicar o remover luego de ser publicados todos aquellos contenidos y/o mensajes propuestos y/o publicados por el Usuario que, a exclusivo criterio de WORKERS, no respondan estrictamente a las disposiciones contenidas en estas Condiciones Generales y/o resulten impropios y/o inadecuados a las características, finalidad y/o calidad de los Servicios.</p>
                        <p>1.6.6. WORKERS no garantiza la disponibilidad y continuidad del funcionamiento de los Canales.</p>
                        <p>1.6.7. WORKERS no es en ningún caso responsable de la destrucción, alteración o eliminación del contenido o información que cada Usuario incluya en sus mensajes.</p>
                        <p>1.6.8. Cada Usuario es único y exclusivo responsable de sus manifestaciones, dichos, opiniones y todo acto que realice a través de los Canales.</p>
                        <p>2. DATOS PERSONALES DEL USUARIO. REGISTRO EN EL SITIO WEB.</p>
                        <p>2.1. Cuando se registre en el Sitio Web, se le pedirá que aporte a WORKERS cierta información que incluirá, entre otras, nombre y apellido, dirección y una dirección válida de correo electrónico, antecedentes académicos y laborales (de corresponder), entre otros.</p>
                        <p>2.2. WORKERS se reserva el derecho de ofrecerle servicios y productos de terceros basados en las preferencias que Usted indicó al momento de registrarse o en cualquier momento posterior; tales ofertas pueden ser efectuadas por WORKERS o por terceros.</p>
                        <p>2.3. Por favor consulte la Política de Privacidad del Sitio Web para conocer los detalles respecto del tratamiento de sus Datos Personales.</p>
                        <p>2.4. El Usuario será responsable de todos los usos de su cuenta, tanto si están autorizados o no por Usted. Usted deberá notificar inmediatamente a WORKERS sobre cualquier uso sin autorización de su cuenta o contraseña.</p>
                        <p>2.5. Los Usuarios registrados y/o que utilicen los Servicios de WORKERS garantizan la veracidad, exactitud, vigencia y autenticidad de la información facilitada, y se comprometen a mantenerlos debidamente actualizados, informando a WORKERS sobre cualquier modificación a través de la sección “Contacto” disponible en la página principal del Sitio Web.</p>
                        <p>2.6. El Usuario autoriza a que la información personal suministrada sea compartida con los clientes de WORKERS.</p>
                        <p>2.7 WORKERS proveerá de a los Usuarios de un contrato modelo de autorización para la calificación y divulgación de datos personales dentro del Sitio Web exclusivamente en la sección “Mi Cuenta” para uso Exclusivo de cada Usuario a entera responsabilidad de aplicación o no del mismo. El Usuario podrá optar por la utilización o no de un modelo propio de autorización de calificación y divulgación de datos personales. La carga de datos, calificaciones y/o divulgación de datos personales sin autorización explicita será responsabilidad exclusiva e intransferible del Usuario.</p>
                        <p>3. MENORES DE EDAD</p>
                        <p>3.1 Queda prohibida la utilización del Sitio Web y/o de los Servicios ofrecidos a través del mismo por personas que carezcan de capacidad legal para contratar o menores de edad según la legislación aplicable conforme la cláusula 12. En caso que ello ocurra, los menores de edad o incapaces deben obtener previamente permiso de sus padres, tutores o representantes legales, quienes serán considerados responsables de todos los actos realizados por las personas a su cargo.</p>
                        <p>3.2 El Sitio Web es una comunidad en constante evolución que incluye diversos usuarios con diferentes experiencias y antecedentes. A pesar que WORKERS puede monitorear y tomar acción sobre la conducta inapropiada de otros usuarios o terceros, en el Sitio Web o el acceso o utilización de cualquiera de los Servicios ofrecidos, incluidas salas de chat y foros públicos, es posible que en cualquier momento pueda presentarse un lenguaje u otros materiales accesibles en o a través de tales que puedan ser inapropiados para menores de edad u ofensivos para otros usuarios. Las presentes Condiciones Generales señalan normas de conducta para los usuarios, pero WORKERS no puede garantizar que el resto de usuarios cumplan con los mismos.</p>
                        <p>3.3 Al acceder al Sitio Web y utilizar los Servicios el Usuario confirma que es mayor de edad.</p>
                        <p>4. RESPONSABILIDAD DEL USUARIO</p>
                        <p>El Usuario declara y acepta que el uso del Sitio Web sus Servicios y los contenidos se efectúan bajo su única y exclusiva responsabilidad.</p>
                        <p>5. EXCLUSIÓN DE GARANTÍAS Y DE RESPONSABILIDAD</p>
                        <p>5.1. Atento al estado de la técnica y a la estructura y funcionamiento de las redes, el Sitio Web no puede confirmar que cada Usuario es quien dice ser.</p>
                        <p>5.2. WORKERS no garantiza la disponibilidad y continuidad del funcionamiento del Sitio Web y de los Servicios ofrecidos. No todos los Servicios y contenidos en general se encuentran disponibles para todas las áreas geográficas. Asimismo, WORKERS no garantiza la utilidad del Sitio Web y de los Servicios para la realización de ninguna actividad en particular, ni su infalibilidad y, en particular, aunque no de modo exclusivo, que los Usuarios puedan efectivamente utilizar el Sitio Web, acceder a las distintas páginas web que forman el Sitio Web o a aquéllas desde las que se prestan los Servicios.</p>
                        <p>5.3. WORKERS no garantiza que el Sitio Web funcione libre de errores o que el Sitio Web y su servidor estén libres de virus informáticos u otros mecanismos lesivos.</p>
                        <p>5.4. El Sitio Web y los Servicios se suministran tal como están, sin garantías de ninguna clase.</p>
                        <p>5.5. WORKERS no garantiza la exactitud, la veracidad, la exhaustividad o la actualización de los contenidos, los Servicios, el software, los textos, los gráficos y los vínculos.</p>
                        <p>5.6. En ningún caso WORKERS será responsable de cualquier daño incluyendo, pero sin limitación, daños directos y/o indirectos, lucro cesante o pérdida de chance que resulten del uso o de la imposibilidad de uso del Sitio Web, sin perjuicio de que WORKERS haya sido advertido sobre la posibilidad de tales daños.</p>
                        <p>5.7. WORKERS excluye toda responsabilidad por los daños y perjuicios de toda naturaleza que pudieran deberse al accionar de terceros no autorizados respecto de los Datos Personales de los Usuarios, así como de los Servicios ofrecidos en el Sitio Web.</p>
                        <p>6. VÍNCULOS A OTROS SITIOS</p>
                        <p>El Sitio Web contiene vínculos a otros sitios de Internet. WORKERS no respalda los contenidos de estos sitios web. WORKERS no es responsable del contenido de los sitios web de terceros y no hace ninguna afirmación relativa al contenido o su exactitud en estos sitios web de terceros. Si Usted decide acceder a sitios web de terceras partes vinculados, lo hace a su propio riesgo.</p>
                        <p>7. CESIÓN O USO COMERCIAL NO AUTORIZADO</p>
                        <p>7.1. Usted acepta no ceder, bajo ningún título, sus derechos u obligaciones bajo estas Condiciones Generales. Usted también acepta no realizar ningún uso comercial no autorizado del Sitio Web.</p>
                        <p>7.2. Asimismo, el Usuario se compromete a utilizar el Sitio Web y los Servicios diligentemente y de conformidad con la ley aplicable y vigente y con estas Condiciones Generales.</p>
                        <p>7. CANCELACIÓN</p>
                        <p>8.1. WORKERS tiene la facultad, y no la obligación, de emitir advertencias, suspensiones temporales y cancelaciones permanentes (baja) de los Usuarios registrados por infracciones a las presentes Condiciones Generales, y/o cualquier aviso, reglamento de uso e instrucción puestos en conocimiento del Usuario por WORKERS.</p>
                        <p>8.2. Sin perjuicio de lo indicado en el punto anterior, WORKERS se reserva el derecho, a su exclusivo criterio, de emplear todos los medios legales a su alcance en caso que Usted infrinja cualquiera de estas Condiciones Generales.</p>
                        <p>9. INDEMNIZACIÓN</p>
                        <p>Usted acepta mantener indemne a WORKERS de y contra cualquier cargo, acción o demanda, incluyendo, pero no limitándose a, los gastos legales razonables, que resulten del uso que Usted haga del Sitio Web, de los Contenidos y de los Servicios. Usted acepta y reconoce que WORKERS es un mero intermediario, ajeno a cualquier relación que pudiera entablarse entre Usted y a quienes se busquen, califiquen y/o divulguen información personal, a través del Sitio Web. En tal sentido, Usted acepta no reclamar a WORKERS por ningún tipo de consecuencia derivada de tales relaciones.</p>
                        <p>10. GENERAL</p>
                        <p>10.1 WORKERS se reserva el derecho a modificar total o parcialmente estas Condiciones Generales en cualquier momento. En caso de llevar a cabo alguna modificación, WORKERS notificará al Usuario a la dirección de correo electrónico registrada para utilizar el Sitio Web y los Servicios. El Usuario acepta que la notificación por parte de WORKERS a dicha dirección de correo electrónico tendrá plena validez como notificación suficiente, y renuncia a cualquier tipo de impugnación respecto de las notificaciones cursadas por WORKERS a tal dirección de correo electrónico. Asimismo, si el Usuario persiste en la utilización de los Servicios y/o el Sitio Web, se considerará que ha aceptado implícitamente las nuevas Condiciones Generales.</p>
                        <p>10.2 En caso de declararse la nulidad de alguna de las cláusulas de estas Condiciones Generales, tal nulidad no afectará a la validez de las restantes, las cuales mantendrán su plena vigencia y efecto.</p>
                        <p>10.3 Estas Condiciones Generales, junto con la Política de Privacidad [INSERTAR LINK A POLÍTICA DE PRIVACIDAD] constituyen la totalidad del acuerdo entre Usted y WORKERS respecto del uso del Sitio Web.</p>
                        <p>11. DURACIÓN Y TERMINACIÓN</p>
                        <p>La prestación del Servicio del Sitio Web tiene una duración indeterminada. Sin perjuicio de lo anterior, WORKERS está autorizada para dar por terminada o suspender la prestación del Servicio del Sitio Web y/o de cualquiera de los Contenidos en cualquier momento.</p>
                        <p>12. LOCALIZACIÓN</p>
                        <p>12.1. ARGENTINA</p>
                        <p>12.1.1. WORKERS significa WORKERS.COM.AR WORKERS S.R.L., CUIT XX-XXXXXXXX-X, con domicilio en xxxxxxxxxxxxxx, Ciudad Autónoma de Buenos Aires.</p>
                        <p>12.1.2. El Sitio Web es www.workers.com.ar.</p>
                        <p>12.1.3. Estas Condiciones Generales se rigen por la las leyes de la República Argentina. El Usuario se somete a la jurisdicción de los Tribunales Ordinarios de la Ciudad de Buenos Aires, con renuncia expresa a cualquier otro fuero y/o jurisdicción.</p>
                        <p>Estos Términos y Condiciones fueron actualizados por última vez el 24 de Julio de 2018.</p>
                    </div>
                </div>
            </section>
        );
    }
}

//export default Terms;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToLogin: () => push('/login')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(Terms)