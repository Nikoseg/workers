import React, { Component } from 'react';
import AuthorizationModal from './../../components/authorizationModal/authorizationModal'

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


class AuthorizationPrint extends Component {

    onAuthReady() {
        if(window){
            window.print();
        }
    }

    render() {
        return (
            <AuthorizationModal controls={false} callback={() => { this.onAuthReady(); }} />
        );
    }
}

//export default AuthorizationPrint;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToAuthorizationPrint: () => push('/auth-print')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(AuthorizationPrint)