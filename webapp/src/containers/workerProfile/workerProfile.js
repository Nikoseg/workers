import React, { Component } from 'react';
import Rodal from 'rodal';

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import ProfilePicture from './../../static/img/profile_picture.png';
import ProgressBar from './../../components/progressBar/progressBar';
import Valorations from './../../components/valorations/valorations';

import APIService from './../../modules/apiService';


class WorkerProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            services: [],
            categories: [],
            qualifications: [],
            visible: false,
            modalTitle: '',
            modalContent: '',
            hasCessationProfile: false,
        };

        this.workerId = props.match.params.id;

        this.api = new APIService();
    }

    getStyles() {
        return { margin: '-30px -60px auto' };
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    componentDidMount() {
        this.api.getWorkerById(this.workerId).then((res)=>{
            this.setState(res.data);
            this.api.getStateById(this.state.state_id).then((res)=>{
                this.setState({
                    stateName: res.data && res.data.name ? res.data.name : ''
                });
                this.api.getCityById(this.state.city_id).then((res)=>{
                    this.setState({
                        cityName: res.data && res.data.name ? res.data.name + ', ' : ''
                    });
                });
            });
        }).catch((err)=>{
            this.showModal('Error', err.message);
        });

        this.api.getWorkerById(this.workerId, '1').then((res)=>{
            if(res.data && res.data.qualifications) {
                // Revisa si tiene calificaciones de Cese Laboral :(
                this.setState({
                    hasCessationProfile: !!res.data.qualifications.length
                });
            }
        }).catch((err)=>{
            this.showModal('Error', err.message);
        });
    }

    render() {

        let servicesDom = this.state.services.map((service) => {
            return <div key={'service' + service.id} className="label btn btn-primary m-2">{service.name}</div>
        });

        let categoriesDom = this.state.categories.map((category) => {
            return <div key={'category' + category.id} className="label btn btn-primary m-2">{category.name}</div>
        });

        let valorationsDom = [];

        this.state.qualifications.forEach((q) => {
            valorationsDom.push(<Valorations key={'q' + q.id} title={q.fantasy_name || q.razon_social} scoring={q.scoring} profileImage={q.full_profile_image} answers={q.answers}/>);
        });

        let globalSkill = [
            {
                max: 10,
                min: 0,
                slider_value: this.state.scoring || 0,
                type: this.state.scoring ? 'slider' : ''
            }
        ];
        return (
            <div>
                <section className="container mb-5 p-lg-5 workers-myaccount">
                    <div className="row justify-content-center">
                        <div className="col-sd-2 pt-4 pb-2 text-center">
                            <div className="workers-profile-image" style={
                                {
                                    backgroundImage: `url('${this.state.full_profile_image ? this.state.full_profile_image : ProfilePicture}')`}
                            }>
                                {/* width="90%" */}
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center workers-profile-user">
                        <div className="col-sd-2 pb-4 text-center">
                            <h1>{this.state.name} {this.state.lastname}</h1>
                            <h3 className="m-2">{this.state.cityName} {this.state.stateName}</h3>
                            <h4 className="m-3"><i className="fa fa-star"></i> {this.state.scoring}</h4>
                            {this.state.hasCessationProfile ?
                                <button className="btn btn-lg btn-primary" onClick={() => this.props.goToCessationProfile(this.workerId)}>
                                    Ver ficha de cese
                                </button>
                                : null
                            }
                        </div>
                    </div>
                    <section className="workers-qualify">
                        <h1 className="m-2">Servicios</h1>
                        <div>
                            {servicesDom}
                        </div>
                    </section>
                    <section className="workers-qualify">
                        <h1 className="m-2">Categorías</h1>
                        <div>
                            {categoriesDom}
                        </div>
                    </section>
                    <section className="workers-qualify">
                        <h1 className="m-2">Scoring</h1>
                        <div className="row">
                            <div className="col-md-6 m-2">
                                <ProgressBar skills={globalSkill}/>
                            </div>
                            <div className="col text-right"><h4><i className="fa fa-star"></i> {this.state.scoring}</h4></div>
                        </div>
                    </section>
                    <section className="workers-qualify">
                        <h1 className="m-2">Valoraciones</h1>
                        <div>
                            {valorationsDom.length > 0 ? valorationsDom : <div className="text-center pt-3">No se encontraron valoraciones</div>}
                        </div>
                    </section>
                </section>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                       className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
            </div>
        );
    }
}

//export default Login;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToLogin: () => push('/login'),
    goToCessationProfile: (id) => push('/cessation-profile/' + id)
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(WorkerProfile)