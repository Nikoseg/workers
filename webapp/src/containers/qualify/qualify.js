import React, { Component } from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import APIService from "../../modules/apiService";

import ProfilePicture from './../../static/img/profile_picture.png'

import UploadProfileFile from './../../components/uploadProfileFile/uploadProfileFile'
import Qualifier from './../../components/qualifier/qualifier'
import FullscreenLoading from './../../components/fullscreenLoading/fullscreenLoading'

import MaskedInput from 'react-text-mask'
import MultiSelect from './../../components/multiSelect/multiSelect'

import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            worker_id: null,
            fullscreenLoadingVisible: false,
            fullscreenLoadingMessage: '',
            spinnerLoadingVisible: false,
            uploadImageError: false,
            file: null,
            imagePreviewUrl: '',
            cuil: '',
            name: '',
            lastname: '',
            states: [],
            state_id: 0,
            cities: [],
            city_id: 0,
            serviceAreas: [],
            area_id: 0,
            services: [], // This is for the view
            servicesAPI: [], // Careful here: this is for the API call
            categories: [], // This is for the view
            categoriesAPI: [], // Careful here: this is for the API call
            end_work: 0,
            qualifications: [],
            visible: false,
            modalTitle: '',
            modalContent: ''
        };

        this.workerId = props.match.params.id;

        this.handleStatesChange = this.handleStatesChange.bind(this);
        this.api = new APIService();

        this.servicesChild = React.createRef();
        this.categoriesChild = React.createRef();

    }

    handleChange(event) {
        this.setState({ [event.target.id]: event.target.value });
    }

    resetCitiesInput() {
        this.setState({
            cities: [],
            city_id: 0,
            spinnerLoadingVisible: false,
        });
    }

    // TODO: Llevar el dropdown de states y cities a uno o dos componentes
    handleStatesChange(event) {
        const state_id = event.target.value;
        this.setState({
            state_id: Number(state_id),
            spinnerLoadingVisible: true
        });
        if (!state_id) {
            this.resetCitiesInput();
            return;
        }
        this.api.getCityByStateId(state_id)
            .then((res) => {
                this.setState({
                    cities: res.data,
                    spinnerLoadingVisible: false
                });
            }).catch(() => {
                this.setState({
                    spinnerLoadingVisible: false
                });
            });
    }

    handleCitiesChange(event) {
        this.setState({
            city_id: Number(event.target.value)
        })
    }

    handleAreasChange(event) {
        this.setState({
            area_id: Number(event.target.value)
        })
    }

    onServicesMultiSelectChange(obj) {
        const apiObj = [];
        obj.forEach((obj) => {
            apiObj.push(Number(obj.id));
        });
        this.setState({
            servicesAPI: apiObj
        });
    }

    onCategoriesMultiSelectChange(obj) {
        const apiObj = [];
        obj.forEach((obj) => {
            apiObj.push(Number(obj.id));
        });
        this.setState({
            categoriesAPI: apiObj
        });
    }

    componentDidMount() {
        this.api.getStates()
            .then((res) => {
                this.setState({
                    states: res.data,
                });
            }).catch(err => {
                this.showModal('Error', err.message);
            });

        this.api.getWorkersAreas()
            .then((res) => {
                this.setState({
                    serviceAreas: res.data,
                });
            }).catch(err => {
                this.showModal('Error', err.message);
            });

        this.api.getServices()
            .then((res) => {
                this.setState({
                    services: res.data,
                });
                //this.servicesChild.current.parentExecution(this.state.services);
            }).catch(err => {
                this.showModal('Error', err.message);
            });

        this.api.getCategories()
            .then((res) => {
                this.setState({
                    categories: res.data,
                });
            }).catch(err => {
                this.showModal('Error', err.message);
            });

        if (this.workerId) {
            this.api.getWorkerById(this.workerId).then((res) => {

                if (res && res.data && res.data.cuil) {
                    const mockedObj = {
                        target: {
                            value: res.data.cuil
                        }
                    };
                    this.onCuilBlur(mockedObj);
                }
            }).catch((err) => {
                this.showModal('No se pudo precargar el perfil', err.message);
            });
        }
    }

    setPreloadedArea(workerId) {
        this.api.getWorkerById(workerId).then((res) => {

            if (res && res.data) {
                if (res.data.areas.length > 0) {
                    this.setState({
                        area_id: res.data.areas[0].id
                    })
                }
            }
        }).catch((err) => {
            this.showModal('No se pudo precargar el area', err.message);
        });
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({
            visible: false,
        });
    }
    //

    sendForm(event) {
        event.preventDefault();
        const sendingData = {
            "cuil": this.state.cuil,
            "name": this.state.name,
            "lastname": this.state.lastname,
            "state_id": this.state.state_id,
            "city_id": this.state.city_id,
            "area_id": this.state.area_id,
            "services": this.state.servicesAPI,
            "categories": this.state.categoriesAPI,
            "end_work": this.state.end_work,
            "qualifications": this.state.qualifications
        };

        console.log(this.state);
        console.log(sendingData);

        this.setState({
            fullscreenLoadingVisible: true
        });

        this.api.postWorker(sendingData)
            .then((res) => {
                this.setState({
                    worker_id: res.data.id
                });
                if (this.state.file) {
                    this.api.postWorkerProfileImage(this.state.file, res.data.id).then(() => {
                        this.props.goToFullscreenMessage(Boolean(this.state.end_work));
                    }).catch((err) => {
                        this.setState({
                            uploadImageError: true,
                        });
                        this.setState({
                            fullscreenLoadingVisible: false
                        });
                        this.showModal('Error', err.message);
                        //this.props.goToFullscreenMessage(Boolean(this.state.end_work));
                    });
                } else {
                    this.props.goToFullscreenMessage(Boolean(this.state.end_work));
                }
            })
            .catch((err) => {
                this.setState({
                    fullscreenLoadingVisible: false
                });
                this.showModal('Error', err.message);
            });
    }

    sendNewImage() {
        this.setState({
            fullscreenLoadingVisible: true
        });
        if (this.state.file && this.state.worker_id) {
            this.api.postWorkerProfileImage(this.state.file, this.state.worker_id).then(() => {
                this.setState({
                    uploadImageError: true,
                });
                this.props.goToFullscreenMessage(Boolean(this.state.end_work));
            }).catch((err) => {
                this.showModal('Error', err.message);
                this.setState({
                    uploadImageError: true,
                    fullscreenLoadingVisible: false
                });
                //this.props.goToFullscreenMessage(Boolean(this.state.end_work));
            });
        } else {
            this.setState({
                uploadImageError: true,
                fullscreenLoadingVisible: false
            });
            this.props.goToFullscreenMessage(Boolean(this.state.end_work));
        }
    }

    // For now, it is only used to preview de profile image...
    updateView(file) {
        let reader = new FileReader();
        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        };

        if (file) {
            reader.readAsDataURL(file)
        }
    }

    onEndWorkChange(end_work) {
        this.setState({
            end_work
        });
    }

    resetQualificationArray() {
        this.setState({
            qualifications: []
        });
    }

    onQualificationsChange(qualification) {
        // Lógica adicional por temas de la API ¬¬
        if (qualification.type === 'slider') {
            qualification.slider_value = qualification.value;
        }
        // Idem anterior
        // "choiSes" porque está así en la API ¬¬
        if (qualification.type === 'choises') {
            qualification.option_value = qualification.value;
        }

        // Idem
        if (qualification.type === 'textarea') {
            qualification.text = qualification.value ? qualification.value : '';
        }

        this.addQualificationToArray(qualification);
    }

    addQualificationToArray(qualification) {
        let qualificationsObj = this.state.qualifications;
        qualificationsObj = qualificationsObj.filter(q => q.id !== qualification.id);
        qualificationsObj.push(qualification);
        this.setState({
            qualifications: qualificationsObj
        });
    }

    onCuilBlur(e) {
        const cuil = e.target.value;
        if (cuil) {
            this.setState({
                fullscreenLoadingVisible: true,
                fullscreenLoadingMessage: 'Buscando Worker preexistente para precarga de datos'
            });
            this.api.getWorkerByCuil(cuil).then((res) => {
                if (res && res.data) {
                    if (res.data.qualifications) {
                        delete res.data.qualifications;
                    }
                    this.setState(res.data);
                    if (res.data.full_profile_image) {
                        this.setState({
                            imagePreviewUrl: res.data.full_profile_image,
                            fullscreenLoadingVisible: false
                        })
                    }

                    if (res.data.services.length > 0) {
                        const preloadedServices = [];
                        res.data.services.forEach((service) => {
                            preloadedServices.push(Number(service.id));
                        });
                        this.setState({
                            servicesAPI: preloadedServices
                        })
                    }
                    if (res.data.categories.length > 0) {
                        const preloadedCategories = [];
                        res.data.categories.forEach((category) => {
                            preloadedCategories.push(Number(category.id));
                        });
                        this.setState({
                            categoriesAPI: preloadedCategories
                        })
                    }

                    const evObj = {
                        target: {
                            value: res.data.state_id
                        }
                    };
                    this.handleStatesChange(evObj); // Force the onChangeEvent

                    this.setPreloadedArea(res.data.id);
                    this.servicesChild.current.refreshComponent(res.data.id);
                    this.categoriesChild.current.refreshComponent(res.data.id);

                } else {
                    this.setState({
                        fullscreenLoadingVisible: false,
                        fullscreenLoadingMessage: ''
                    })
                }
            }).catch(() => {
                this.setState({
                    fullscreenLoadingVisible: false,
                    fullscreenLoadingMessage: ''
                });
            });
        }
    }

    render() {

        let states = this.state.states;
        let statesOptionItems = states.map((state) =>
            <option key={state.id} value={state.id}>{state.name}</option>
        );

        let cities = this.state.cities;
        let citiesOptionItems = cities.map((city) =>
            <option key={city.id} value={city.id}>{city.name}</option>
        );

        let serviceAreas = this.state.serviceAreas;
        let serviceAreasOptionItems = serviceAreas.map((area) =>
            <option key={area.id} value={area.id}>{area.name}</option>
        );

        let loadingSpinnerNotVisible = 'fa fa-spinner fa-2x fa-fw fa-spin text-primary select-spin-loading d-none';
        let loadingSpinnerVisible = 'fa fa-spinner fa-2x fa-fw fa-spin text-primary select-spin-loading';

        return (
            <div className="container">
                <form>
                    <section className="workers-qualify-profile-data">
                        <div className="row">
                            <div className="col">
                                <h1>Valoración</h1>
                            </div>
                        </div>
                        <div className="row justify-content-center">
                            <div className="col-sd-2 p-4 text-center">
                                <div className="workers-profile-image" style={
                                    {
                                        backgroundImage: `url('${this.state.imagePreviewUrl ? this.state.imagePreviewUrl : ProfilePicture}')`,
                                        width: '120px',
                                        height: '120px'
                                    }
                                }>
                                    {/* width="70%" */}
                                </div>
                                <div className="upload-profile-wrapper text-right">
                                    <UploadProfileFile preview={true} updateListener={this.updateView.bind(this)} />
                                </div>
                            </div>
                        </div>
                    </section>
                    {this.state.uploadImageError ?

                        <div className="row justify-content-center text-center">
                            <div className="col-auto">
                                <button className="btn btn-lg btn-primary m-2" onClick={() => {
                                    this.sendNewImage();
                                }}>Enviar nueva imagen</button>
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-lg btn-outline-primary m-2"
                                    onClick={() => { this.props.goToFullscreenMessage(Boolean(this.state.end_work)) }}>Calificar sin imagen</button>
                            </div>
                        </div>

                        :
                        <div>
                            <section className="workers-qualify-profile-data container">
                                <div class="row">
                                    <div class="col">
                                        <div className="row justify-content-start">
                                            <div className="col-sd-12 col-md-12">
                                                <div className="form-group">
                                                    <MaskedInput
                                                        className="form-control" id="cuil" type="text" placeholder="CUIT / CUIL"
                                                        required="required"
                                                        value={this.state.cuil} onChange={this.handleChange.bind(this)}
                                                        onBlur={this.onCuilBlur.bind(this)}
                                                        mask={[/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col-sd-12 col-md-6">
                                                <div className="form-group">
                                                    <input
                                                        className="form-control"
                                                        type="text"
                                                        placeholder="Nombre"
                                                        required="required"
                                                        id="name"
                                                        value={this.state.name} onChange={this.handleChange.bind(this)}
                                                    />
                                                </div>
                                            </div>
                                            <div className="col-sd-12 col-md-6">
                                                <div className="form-group">
                                                    <input
                                                        className="form-control"
                                                        type="text"
                                                        placeholder="Apellido"
                                                        required="required"
                                                        id="lastname"
                                                        value={this.state.lastname} onChange={this.handleChange.bind(this)}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-row mb-3">
                                            <div className="col-sd-12 col-md-12">
                                                <select
                                                    className="form-control custom-select"
                                                    required="required"
                                                    value={this.state.state_id}
                                                    onChange={this.handleStatesChange.bind(this)}
                                                >
                                                    <optgroup>
                                                        <option value="">Provincia</option>
                                                        {statesOptionItems}
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-row mb-3">
                                            <div className="col-sd-12 col-md-12">
                                                <select
                                                    className="form-control custom-select"
                                                    required="required"
                                                    disabled={this.state.spinnerLoadingVisible}
                                                    value={this.state.city_id}
                                                    onChange={this.handleCitiesChange.bind(this)}
                                                >
                                                    <optgroup>
                                                        <option value="">Ciudad</option>
                                                        {citiesOptionItems}
                                                    </optgroup>
                                                </select>
                                                <i className={this.state.spinnerLoadingVisible ? loadingSpinnerVisible : loadingSpinnerNotVisible}></i>
                                            </div>
                                        </div>
                                        <div className="form-row mb-3">
                                            <div className="col-sd-12 col-md-12">
                                                <select
                                                    className="form-control custom-select"
                                                    required="required"
                                                    value={this.state.area_id}
                                                    onChange={this.handleAreasChange.bind(this)}
                                                >
                                                    <optgroup>
                                                        <option value="">Área</option>
                                                        {serviceAreasOptionItems}
                                                    </optgroup>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="workers-qualify-profile-data__divider" />
                                    <div class="col">
                                        <div className="d-flex flex-column h-100 justify-content-between">
                                            <div>
                                                <div className="form-row mb-3">
                                                    <div className="col-sd-12 col-md-12">
                                                        <MultiSelect
                                                            placeholder="Servicios"
                                                            onMultiSelectChange={this.onServicesMultiSelectChange.bind(this)}
                                                            workerId={this.workerId}
                                                            preset={'services'}
                                                            ref={this.servicesChild}
                                                            endpoint={this.api.getServices.bind(this)}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="form-row mb-3">
                                                    <div className="col-sd-12 col-md-12">
                                                        <MultiSelect
                                                            placeholder="Cargo"
                                                            onMultiSelectChange={this.onCategoriesMultiSelectChange.bind(this)}
                                                            workerId={this.workerId}
                                                            preset={'categories'}
                                                            ref={this.categoriesChild}
                                                            endpoint={this.api.getCategories.bind(this)}
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                            {/* We need to review how we want to implement this section
                                            because has logic in qualifier component */}
                                            <div className="row justify-content-start flex-column mb-3">
                                                <div className="col-sd-12 col-md-12">
                                                    <h2 className="mb-3">Está activo laboralmente:</h2>
                                                    <div className="row mx-auto justify-content-center workers-qualify-profile-data__button-container">
                                                        <div
                                                            className={
                                                                this.state.end_work === 1
                                                                    ? "btn btn-primary m-1 btn-lg workers-qualify-profile-data__button"
                                                                    : "btn btn-outline-primary m-1 btn-lg workers-qualify-profile-data__button"
                                                            }
                                                            onClick={() => this.onEndWorkChange(1)}
                                                        >
                                                            Sí
                                                        </div>
                                                        <div
                                                            className={
                                                                this.state.end_work === 0
                                                                    ? "btn btn-primary m-1 btn-lg workers-qualify-profile-data__button"
                                                                    : "btn btn-outline-primary m-1 btn-lg workers-qualify-profile-data__button"
                                                            }
                                                            onClick={() => this.onEndWorkChange(0)}
                                                        >
                                                            No
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <Qualifier
                                endWork={this.state.end_work}
                                editionMode={true}
                                onEndWorkChange={this.onEndWorkChange.bind(this)}
                                resetQualificationArray={this.resetQualificationArray.bind(this)}
                                onQualificationsChange={this.onQualificationsChange.bind(this)}
                            />
                            <section className="workers-qualify">
                                <div className="row justify-content-center">
                                    <div className="col-auto">
                                        <button className="btn btn-lg btn-primary btn-send" onClick={(e) => this.sendForm(e)}>Enviar</button>
                                    </div>
                                </div>
                            </section>
                        </div>
                    }
                </form>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                    className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
                <FullscreenLoading visible={this.state.fullscreenLoadingVisible} text={this.state.fullscreenLoadingMessage} />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: () => push('/about-us'),
    goToFullscreenMessage: (end_work) => push('/fullscreen-msg/' + end_work)
}, dispatch)

export default connect(
    null,
    mapDispatchToProps
)(Search)