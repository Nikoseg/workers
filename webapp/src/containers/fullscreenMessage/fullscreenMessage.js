import React, { Component } from 'react';
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class FullscreenMessage extends Component {

    constructor(props) {
        super(props);

        this.end_work = props.match.params.end_work ? props.match.params.end_work : false;
    }

    render() {

        let message = '';
        if(this.end_work ==='true') { // Porque es un string, lo buscamos '==='
            message = 'Su valoración ha sido cargada con éxito. A partir de este momento su calificación podrá ser visualizada por otros usuarios.';
        } else {
            message = 'Su valoración ha sido cargada con éxito en su cuenta. Le recordamos que esta publicación será Privada hasta tanto se indique el cese laboral.';
        }

        return(
            <section className="container mb-5 p-lg-5 workers-fullscreen-msg">
                <div className="row justify-content-center">
                    <div className="col-auto mb-3">
                        <h1>¡Gracias por calificar!</h1>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col text-center mb-3">
                        <p>{message}</p>
                    </div>
                </div>
                <div className="col-sd-2 pb-4 text-center">
                    <a className="btn btn-lg btn-primary" href='/auth-print' target='_blank'>
                        Autorización
                    </a>
                </div>
                <div className="row justify-content-center">
                    <div className="col text-center mb-3">
                        <p>Puedes continuar calificando a tus empleados o comienza a buscar nuevos trabajadores para contratar</p>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-auto mb-3">
                        <button
                            className="btn btn-lg btn-primary"
                            onClick={() => this.props.goToQualify()}>Calificar</button>
                    </div>
                    <div className="col-auto mb-3">
                        <button
                            className="btn btn-lg btn-outline-primary"
                            onClick={() => this.props.goToSearch()}>Buscar</button>
                    </div>
                </div>
            </section>
        );
    }
}

//export default FullscreenMessage;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToQualify: () => push('/qualify'),
    goToSearch: () => push('/search')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(FullscreenMessage)