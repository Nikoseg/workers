import React, {Component} from 'react'
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import APIService from "../../modules/apiService";
import Rodal from 'rodal';
import SearchResults from "../../components/searchResults/searchResults";
import FullscreenLoading from './../../components/fullscreenLoading/fullscreenLoading'

class Search extends Component {

    constructor() {
        super();

        this.state = {
            fullscreenLoadingVisible: false,
            spinnerLoadingVisible: false,
            queryString: '',
            states: [],
            stateId: 0,
            cities: [],
            cityId: 0,
            serviceAreas: [],
            areaId: 0,
            orderBy: 'scoring',
            visible: false,
            modalTitle: '',
            modalContent: '',
            searchingResult: null,
            paging: null,
            activePage: 1,
        };

        this.handleStatesChange = this.handleStatesChange.bind(this);
        this.api = new APIService();
    }

    resetCitiesInput() {
        this.setState({
            cities: [],
            cityId: 0,
            spinnerLoadingVisible: false,
        });
    }

    handleQueryStringChange(event){
        const query = event.target.value;
        this.setState({
            queryString: query
        });
    }

    // TODO: Llevar el dropdown de states y cities a uno o dos componentes
    handleStatesChange(event) {
        const state_id = Number(event.target.value);
        this.setState({
            stateId: state_id,
            spinnerLoadingVisible: true,
        });
        if(!state_id){
            this.resetCitiesInput();
            return;
        }
        this.api.getCityByStateId(state_id)
            .then((res) => {
                this.setState({
                    cities: res.data,
                    spinnerLoadingVisible: false,
                });
            }).catch(err => {
            this.showModal('Error', err.message);
            this.setState({
                spinnerLoadingVisible: false,
            });
        });
    }

    handleCitiesChange(event) {
        const city_id = event.target.value;
        this.setState({
            cityId: Number(city_id)
        });
    }

    handleAreasChange(event) {
        const area_id = event.target.value;
        this.setState({
            areaId: Number(area_id)
        });
    }

    handleOrderByChange(event) {
        const order_by = event.target.value;
        this.setState({
            orderBy: order_by
        });
    }

    componentDidMount() {
        this.api.getStates()
            .then((res) => {
                console.log(res);
                //this.state.states = res.data;
                this.setState({
                    states: res.data,
                });
            }).catch(err => {
                this.showModal('Error', err.message)
        });
        this.api.getWorkersAreas()
            .then((res) => {
                this.setState({
                    serviceAreas: res.data,
                });
            }).catch(err => {
            this.showModal('Error', err.message)
        });
    }


    parseResults(results) {
        return new Promise((resolve, reject) => {
            let workerCount = results && results.length ? results.length : [];
            results.map((worker, index) => {
                this.api.getStateById(worker.state_id).then((res)=>{
                    results[index].state = res.data;
                    this.api.getCityById(worker.city_id).then((res)=>{
                        results[index].city = res.data;
                        workerCount--;
                        if(workerCount === 0){
                            resolve(results);
                        }
                    })

                }).catch((err)=>{
                    reject(err);
                });
            });
            if(workerCount.length === 0) {
                resolve([]);
            }
        });
    }

    search(page) {

        if(page) {
            this.setState({
                activePage: page
            });
        }

        if(!page){
            this.setState({
                activePage: 1
            });
        }

        this.setState({
            searchingResults: null,
            fullscreenLoadingVisible: true,
        });

        // Después se puede aplicar esto ->
        // https://medium.freecodecamp.org/how-to-conditionally-build-an-object-in-javascript-with-es6-e2c49022c448
        const options = {
            "limit":3,
            "offset":0,
            "order_by": "name",
            "order_by_direction": this.state.orderBy === 'scoring' ? "DESC" : "ASC"
        };

        if(page) {
            options.offset = (page - 1) * options.limit;
        }

        if(this.state.queryString){
            options.q = this.state.queryString;
        }

        if(this.state.stateId) {
            options.state_id = this.state.stateId;
        }

        if(this.state.cityId) {
            options.city_id = this.state.cityId;
        }

        if(this.state.areaId) {
            options.area_id = this.state.areaId;
        }

        if(this.state.orderBy !== '') {
            options.order_by = this.state.orderBy;
        }

       this.api.searchWorkers(options).then((res)=>{
           const paging = res.paging;
           this.parseResults(res.data).then((res)=>{
               console.log(res);
               this.setState({
                   searchingResult: res,
                   fullscreenLoadingVisible: false,
                   paging
               })
           })
       }).catch((err)=>{
           this.showModal('Error', err.message)
       });
    }

    handlePageChange(pageNumber) {
        this.search(pageNumber);
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    render() {

        let states = this.state.states;
        let statesOptionItems = states.map((state) =>
            <option key={state.id} value={state.id}>{state.name}</option>
        );

        let cities = this.state.cities;
        let citiesOptionItems = cities.map((city) =>
            <option key={city.id} value={city.id}>{city.name}</option>
        );

        let serviceAreas = this.state.serviceAreas;
        let serviceAreasOptionItems = serviceAreas.map((area) =>
            <option key={area.id} value={area.id}>{area.name}</option>
        );

        let loadingSpinnerNotVisible = 'fa fa-spinner fa-fw fa-spin text-primary select-spin-loading d-none';
        let loadingSpinnerVisible = 'fa fa-spinner fa-fw fa-spin text-primary select-spin-loading';

        return (
            <div>
                <section className="container workers-search">
                    <div className="row">
                        <div className="col">
                            <h1>Buscar</h1>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                <input className="form-control" type="text" placeholder="Buscar por Nombre, CUIT o Servicio..."
                                       onChange={this.handleQueryStringChange.bind(this)} />
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <label className="separator">Filtrar por</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12 col-md-3 mb-3 mb-md-0">
                            <select className="form-control" onChange={this.handleStatesChange}>
                                <optgroup>
                                    <option value="0" selected>Provincia</option>
                                    {statesOptionItems}
                                </optgroup>
                            </select>
                        </div>
                        <div className="col-sm-12 col-md-3 mb-3 mb-md-0">
                            <select className="form-control"
                                    disabled={this.state.spinnerLoadingVisible}
                                    onChange={this.handleCitiesChange.bind(this)}>
                                <optgroup>
                                    <option value="0">Ciudad</option>
                                    {citiesOptionItems}
                                </optgroup>
                            </select>
                            <i className={this.state.spinnerLoadingVisible ? loadingSpinnerVisible : loadingSpinnerNotVisible}></i>
                        </div>
                        <div className="col-sm-12 col-md-3 mb-3 mb-md-0">
                            <select className="form-control" onChange={this.handleAreasChange.bind(this)}>
                                <optgroup>
                                    <option value="0">Área</option>
                                    {serviceAreasOptionItems}
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <label className="separator">Ordenar por</label>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-12 col-md-3 mb-3 mb-md-0">
                            <select className="form-control"
                                    value={this.state.orderBy}
                                    onChange={this.handleOrderByChange.bind(this)}>
                                <optgroup>
                                    <option value="name" selected>Nombre</option>
                                    <option value="scoring">Scoring</option>
                                    <option value="lastname">Apellido</option>
                                </optgroup>
                            </select>
                        </div>
                        <div className="col-12 mb-md-0 mb-3 mt-3" id="btnSearchContainer">
                            <button className="btn btn-lg btn-primary" onClick={()=> this.search()}>Buscar</button>
                        </div>
                    </div>
                </section>
                <section className="container mb-5">
                    <FullscreenLoading visible={this.state.fullscreenLoadingVisible} text={'Buscando...'}/>
                    { this.state.searchingResult !== null ? this.state.searchingResult.length > 0 ?
                        <SearchResults results={this.state.searchingResult}
                                       paging={this.state.paging}
                                       activePage={this.state.activePage}
                                       handlePageChange={this.handlePageChange.bind(this)}
                        /> : <h1>No se encontraron resultados</h1> : null }
                </section>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <p>{this.state.modalContent}</p>
                </Rodal>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({
    changePage: () => push('/about-us')
}, dispatch)

export default connect(
    null,
    mapDispatchToProps
)(Search)