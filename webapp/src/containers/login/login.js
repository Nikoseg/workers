import React, { Component } from 'react';
import Rodal from 'rodal';

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import AuthService from "../../modules/authService";

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            name: '',
            visible: false,
            modalTitle: '',
            modalContent: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.auth = new AuthService();
    }

    handleChange(event) {
        this.setState({[event.target.id]: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.login();
    }

    login() {
        this.auth.login(this.state.email, this.state.password)
            .then(() => {
                this.successLoginRedirect();
            }).catch(err => {
            this.showModal('Error', err.message);
        });
    }

    successLoginRedirect() {
        this.props.goToMyAccount();
    }

    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }

    /* MODAL Functions */
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //
    getStyles() {
        return { margin: '-30px -60px auto' };
    }

    componentDidMount () {
        this.auth.checkLoginStatusAndDoSomethingOrDefault(this.props.goToMyAccount, ()=>{});
    }

    render() {
        return (
            <div style={this.getStyles()}>
                <header className="masthead text-center">
                    <section className="container col-md-4 bg-white mb-5 p-5 workers-login-form">
                            <div className="row">
                                <div className="col-lg-12 text-center">
                                    <h2 className="section-heading mb-2">Acceder</h2>
                                </div>
                            </div>
                            <div className="row justify-content-center">
                                <div className="col-lg-9">
                                    <p>Escriba su usuario y contraseña para ingresar</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col">
                                    <form id="contactForm" name="sentMessage" noValidate="novalidate" onSubmit={this.handleSubmit}>
                                        <div className="row">
                                            <div className="col-md-12">
                                                <div className="form-group">
                                                    <input className="form-control" id="email" type="email" placeholder="Email"
                                                           required="required"
                                                           value={this.state.email} onChange={this.handleChange}
                                                           data-validation-required-message="Please enter your email address." />
                                                </div>
                                                <div className="form-group">
                                                    <input className="form-control" id="password" type="password" placeholder="Contraseña"
                                                           required="required"
                                                           value={this.state.password} onChange={this.handleChange}
                                                           data-validation-required-message="Please enter your password" />
                                                </div>
                                            </div>
                                            <div className="col-lg-12 text-center">
                                                <button id="sendMessageButton"
                                                        className="btn btn-lg btn-primary btn-xl text-uppercase mt-2"
                                                type="submit">Ingresar
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col text-center">
                                    <div className="col-lg-12 text-center mt-3">
                                        <a onClick={() => this.props.goToRegister()}>Registrarse</a>
                                    </div>
                                    <div className="col-lg-12 text-center">
                                        <a className="nav-link disabled text-black-50" onClick={() => this.props.goToPasswordRequest()}>¿Olvidaste tu contraseña?</a>
                                    </div>
                                </div>
                            </div>
                            <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                                <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                                <p>{this.state.modalContent}</p>
                            </Rodal>
                    </section>
                </header>
                <footer className="footer bg-dark">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 h-100 text-center my-auto">
                                {/*<ul className="list-inline mb-0">
                                    <li className="list-inline-item mr-3">
                                        <a className="text-white">
                                            <i className="fa fa-facebook fa-2x fa-fw"></i>
                                        </a>
                                    </li>
                                    <li className="list-inline-item mr-3">
                                        <a className="text-white">
                                            <i className="fa fa-twitter fa-2x fa-fw"></i>
                                        </a>
                                    </li>
                                    <li className="list-inline-item">
                                        <a className="text-white">
                                            <i className="fa fa-instagram fa-2x fa-fw"></i>
                                        </a>
                                    </li>
                                </ul>*/}
                                <p className="text-muted small mb-4 mb-lg-0">&copy; Workers 2018. Todos los derechos reservados.</p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
                );
    }
}

//export default Login;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToMyAccount: () => push('/my-factory'),
    goToPasswordRequest: () => push('/password-request'),
    goToRegister: () => push('/register')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(Login)