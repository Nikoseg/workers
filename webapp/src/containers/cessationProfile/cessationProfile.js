import React, { Component } from 'react';

import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import ProfilePicture from './../../static/img/profile_picture.png';
import APIService from "../../modules/apiService";

import Valorations from './../../components/valorations/valorations';

class CessationProfile extends Component {

    constructor(props){
        super(props);

        super(props);
        this.state = {
            services: [],
            categories: [],
            qualifications: [],
            visible: false,
            modalTitle: '',
            modalContent: '',
        };

        this.workerId = props.match.params.id;

        this.api = new APIService();
    }


    componentDidMount(){
        this.api.getWorkerById(this.workerId, '1').then((res)=>{
            this.setState(res.data);
            this.api.getStateById(this.state.state_id).then((res)=>{
                this.setState({
                    stateName: res.data && res.data.name ? res.data.name : ''
                });
                this.api.getCityById(this.state.city_id).then((res)=>{
                    this.setState({
                        cityName: res.data && res.data.name ? res.data.name + ', ' : ''
                    });
                });
            });
        }).catch((err)=>{
            this.showModal('Error', err.message);
        });
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }
    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    render() {

        let valorationsDom = [];

        this.state.qualifications.forEach((q) => {
            valorationsDom.push(<Valorations key={'q' + q.id} title={q.fantasy_name || q.razon_social} scoring={q.scoring} profileImage={q.full_profile_image} answers={q.answers}/>);
        });

        return(
            <section className="container mb-5 p-lg-5 workers-myaccount">
                <div className="row justify-content-center">
                    <div className="col-sd-2 pt-4 pb-2 text-center">
                        <div className="workers-profile-image" style={
                            {
                                backgroundImage: `url('${this.state.full_profile_image ? this.state.full_profile_image : ProfilePicture}')`}
                        }>
                            {/* width="90%" */}
                        </div>
                    </div>
                </div>
                <div className="row justify-content-center workers-profile-user">
                    <div className="col-sd-2 pb-4 text-center">
                        <h1>{this.state.name} {this.state.lastname}</h1>
                        <h3 className="m-2">{this.state.cityName} {this.state.stateName}</h3>
                        <h4 className="m-3"><i className="fa fa-star"></i> {this.state.scoring}</h4>
                    </div>
                </div>
                <div className="row">
                    <div className="col mb-3">
                        <h1>Ficha de cese</h1>
                    </div>
                </div>
                <section className="workers-qualify">
                    <div>
                        {valorationsDom.length > 0 ? valorationsDom : <div className="text-center pt-3">No se encontraron valoraciones</div>}
                    </div>
                </section>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                       className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
            </section>
        );
    }
}

export default CessationProfile;