import React, { Component } from 'react';
import Rodal from 'rodal';
import MaskedInput from 'react-text-mask'

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import APIService from "../../modules/apiService";

import AuthService from "../../modules/authService";

import FullscreenLoading from './../../components/fullscreenLoading/fullscreenLoading'

class Register extends Component {

    /*
    * - Nombre *
- Apellido *
- cuit *
- Nombre de la empresa *
- Razón social
- Email *
- Contraseña *
*/

    constructor(props) {
        super(props);
        this.state = {
            fullscreenLoadingVisible: false,
            name: '',
            lastname: '',
            cuit: '',
            fantasy_name: '',
            email: '',
            password: '',
            visible: false,
            modalTitle: '',
            modalContent: '',
            modalRedirect: false,
            hasReadTerms: false
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);

        this.api = new APIService();

        this.auth = new AuthService();
    }

    handleChange(event) {
        this.setState({[event.target.id]: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        if(!this.state.hasReadTerms) {
            this.showModal('Error', 'Usted debe aceptar los Términos y Condiciones para poder registrarse', false);
            return;
        }
        this.register();
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;

        this.setState({
            hasReadTerms: value
        });
    }

    register() {
        this.setState({
            fullscreenLoadingVisible: true
        });
        this.api.registerUser(this.state)
            .then((res) => {
                this.setState({
                    fullscreenLoadingVisible: false
                });
                this.showModal('Éxito', res.message, true);
            }).catch(err => {
            this.setState({
                fullscreenLoadingVisible: false
            });
            this.showModal('Error', err.message, false);
        });
    }

    successRegisterRedirect() {
        this.props.goToLogin();
    }

    showModal(title, content, modalRedirect) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.setState({
            modalRedirect
        });
        this.show();
    }

    /* MODAL Functions */
    show() {
        this.setState({ visible: true });
    }

    hide() {
        if(this.state.modalRedirect) {
            this.setState({
                modalRedirect: false
            });
            this.successRegisterRedirect();
        } else {
            this.setState({ visible: false });
        }
    }
    //

    getStyles() {
        return { margin: '-30px -60px auto' };
    }

    componentDidMount () {
        this.auth.checkLoginStatusAndDoSomethingOrDefault(()=>{this.auth.logout()}, ()=>{});
    }

    render() {
        return (
            <div style={this.getStyles()}>
                <header className="masthead text-center">
                    <section className="container col-md-5 bg-white mb-5 p-5 workers-login-form">
                        <div className="row">
                            <div className="col-lg-12 text-center">
                                <h2 className="section-heading mb-5">Registro</h2>
                                <p>Ingrese sus datos para crear una cuenta nueva</p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <form id="contactForm" name="sentMessage" noValidate="novalidate" onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <div className="form-group">
                                                <input className="form-control" id="name" type="text" placeholder="Nombre"
                                                       required="required"
                                                       value={this.state.name} onChange={this.handleChange}
                                                       data-validation-required-message="Please enter your password" />
                                            </div>
                                            <div className="form-group">
                                                <input className="form-control" id="lastname" type="text" placeholder="Apellido"
                                                       required="required"
                                                       value={this.state.lastname} onChange={this.handleChange}
                                                       data-validation-required-message="Please enter your password" />
                                            </div>
                                            <div className="form-group">
                                                <MaskedInput
                                                    className="form-control" id="cuit" type="text" placeholder="CUIT"
                                                    required="required"
                                                    value={this.state.cuit} onChange={this.handleChange}
                                                    mask={[/\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/]}
                                                />
                                            </div>
                                            <div className="form-group">
                                                <input className="form-control" id="fantasy_name" type="text" placeholder="Nombre de la Empresa"
                                                       required="required"
                                                       value={this.state.fantasy_name} onChange={this.handleChange}
                                                       data-validation-required-message="Please enter your password" />
                                            </div>
                                            <div className="form-group">
                                                <input className="form-control" id="email" type="email" placeholder="Email"
                                                       required="required"
                                                       value={this.state.email} onChange={this.handleChange}
                                                       data-validation-required-message="Please enter your email address." />
                                            </div>
                                            <div className="form-group">
                                                <input className="form-control" id="password" type="password" placeholder="Contraseña"
                                                       required="required"
                                                       value={this.state.password} onChange={this.handleChange}
                                                       data-validation-required-message="Please enter your password" />
                                            </div>
                                        </div>
                                        <div className="col-lg-12 text-center">
                                            <label>
                                                <input
                                                    type="checkbox"
                                                    checked={this.state.hasReadTerms}
                                                    onChange={this.handleInputChange}
                                                /> Acepto los <a target="_blank" href="/terms">Términos y Condiciones</a>
                                            </label>
                                        </div>
                                        <div className="col-lg-12 text-center">
                                            <button id="sendMessageButton"
                                                    className="btn btn-lg btn-primary btn-xl text-uppercase mt-5"
                                                    type="submit">Registrarse
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-lg-12 text-center mt-3">
                                <a onClick={() => this.props.goToLogin()}>Ya tengo cuenta</a>
                            </div>
                        </div>
                        <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}>
                            <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                            <p>{this.state.modalContent}</p>
                        </Rodal>
                    </section>
                </header>
                <FullscreenLoading visible={this.state.fullscreenLoadingVisible}/>
            </div>
        );
    }
}

//export default Login;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToLogin: () => push('/login'),
    goToMyAccount: () => push('/my-account')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(Register)