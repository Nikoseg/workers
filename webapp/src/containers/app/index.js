import React, {Component} from 'react';
import { Route } from 'react-router-dom'
import Login from '../login/login'
import PasswordRequest from '../passwordRequest/passwordRequest'
import Register from '../register/register'
import Search from '../search/search'
import Qualify from '../qualify/qualify'
import MyAccount from '../MyAccount/MyAccount'
import MyFactory from '../MyFactory/MyFactory'
import Terms from '../terms/terms'
import AuthorizationPrint from '../authorizationPrint/authorizationPrint'
import WorkerProfile from '../workerProfile/workerProfile'
import CessationProfile from '../cessationProfile/cessationProfile'
import FullscreenMessage from '../fullscreenMessage/fullscreenMessage'
import './../../static/css/landing-page.css'
import './../../css/webapp.css'
import Header from './../../components/header/header';
import Footer from './../../components/footer/footer'

class App extends Component {

    // TODO: Revisar después esto...
    getStyles() {
        let mainStyle = {
            //padding: '100px 60px 0'
            padding: '60px 60px 0'
        };

        if(window && window.innerWidth) {
            const windowWidth = window.innerWidth;
            if(windowWidth < 640) {
                mainStyle = {
                    padding: '80px 10px 0'
                };
            }
        }

        return mainStyle;

    }

    render () {
        return (
            <div>
                <Header/>
                    <main style={this.getStyles()}>
                        <Route exact path="/" component={Login}/> {/* REVISAR */}
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/password-request" component={PasswordRequest}/>
                        <Route exact path="/search" component={Search}/>
                        <Route exact path="/qualify" component={Qualify}/>
                        <Route exact path="/qualify/:id" component={Qualify}/>
                        <Route exact path="/my-account" component={MyAccount}/>
                        <Route exact path="/my-factory" component={MyFactory}/>
                        <Route exact path="/register" component={Register}/>
                        <Route exact path="/terms" component={Terms}/>
                        <Route exact path="/auth-print" component={AuthorizationPrint}/>
                        <Route exact path="/worker-profile/:id" component={WorkerProfile}/>
                        <Route exact path="/cessation-profile/:id" component={CessationProfile}/>
                        <Route exact path="/fullscreen-msg/:end_work" component={FullscreenMessage}/>
                        <Route exact path="/fullscreen-msg/" component={FullscreenMessage}/>
                        {/* <Route path="*" component={Login}/> REVISAR para meterle un 404 */}
                    </main>
                <Footer/>
            </div>
        )
    }
}

export default App;
