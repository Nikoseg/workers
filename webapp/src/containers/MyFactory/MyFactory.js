import React, { Component } from 'react';
import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import './../../modules/authService';

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AuthService from "../../modules/authService";

import ProfilePicture from './../../static/img/profile_picture.png';
import UploadProfileFile from './../../components/uploadProfileFile/uploadProfileFile';

import imgRating from './../../static/img/StarRating.png'

import QualificationListProfile from "../../components/qualificationListProfile/qualificationListProfile";
import ChangePassword from "../../components/changePassword/changePassword";

// API SERVICE
import APIService from './../../modules/apiService';

class MyFactory extends Component {

    constructor(props) {
        super(props);

        this.state = {
            scoring_previous: '0.0',
            scoring: '0.0',
            workers: [],
            workersListDom: [],
        };

        this.auth = new AuthService();
        this.api = new APIService();
    }

    logout() {
        this.auth.logout(); // clears user data
        this.auth.setHeaderInOrderOfUserStatus();
        this.props.goToLogin();
    }

    updateView() {
        this.componentDidMount();
    }

    eraseEmptyWorkers(workers){
        const filteredWorkers = workers.filter((worker) => worker.qualifications && worker.qualifications.length > 0);
        this.renderWorkers(filteredWorkers)
    }

    renderWorkers(workers) {
        if(!workers) {
            return;
        }
        let workersListDom = workers.map((worker) =>
            <QualificationListProfile key={worker.id + Math.random()}
                                      id={worker.id + Math.random()}
                                      worker={worker}
                                      updateView={this.updateView.bind(this)}
            />
        );
        this.setState({
            workersListDom
        });
    }

    updateCompanyData() {
        this.api.getMyCompanies().then((res)=>{
            let mergedObj = Object.assign({}, this.state, res.data);
            this.setState(mergedObj);
            this.eraseEmptyWorkers(mergedObj.workers);
        }).catch((err)=>{
            //this.showModal('Error', err.message);
            console.log(err.message);
        });
    }

    // @TODO: Implementar
    componentDidMount() {
        this.api.getUserMe()
            .then((res)=>{
                delete res.data.full_profile_image; // Avoid to step a valid image with a null uri
                let mergedObj = Object.assign({}, this.state, res.data);
                this.setState(mergedObj);
                this.updateCompanyData();
            }).catch((err)=>{
                this.showModal('Error', err.message);
            });
    }

    changePassword() {
        this.showModal('Cambiar contraseña', <ChangePassword/>);
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    getScoringArrow() {
        const upArrow = 'fa fa-arrow-up text-primary';
        const downArrow = 'fa fa-arrow-down text-danger';
        const equalArrow = 'fa fa-arrow-right';

        if(this.state.scoring < this.state.scoring_previous) {
            return downArrow;
        }
        if(this.state.scoring > this.state.scoring_previous) {
            return upArrow;
        }
        if(this.state.scoring === this.state.scoring_previous) {
            return equalArrow;
        }
        return equalArrow; // default
    }

    // TODO: Revisar después esto...
    getRodalSize() {
        debugger;
        let size = '';
        if(window && window.innerWidth) {
            const windowWidth = window.innerWidth;
            if(windowWidth < 800) {
                size = '100%';
            }
        }

        return size;

    }

    render() {
        const equalArrow = 'fa fa-arrow-right';

        const controlsClassname = 'btn btn-large btn-lg btn-primary';

        let scoringArrow = equalArrow;
        scoringArrow = this.getScoringArrow();

        return (
            <div>
                <section className="container mb-5 p-lg-5 workers-myaccount">
                    <div className="row mb-5">
                        <div className="col">
                            <div className="h1-factory-dropdown mb-5">
                                <h1 className="h1-factory">{this.state.fantasy_name}</h1>
                                <div className="dropdown-content">
                                    <p><a onClick={() => this.changePassword()}>Cambiar contraseña</a></p>
                                    <p><a onClick={() => this.logout()}>Cerrar sesión</a></p>
                                </div>
                            </div>
                            <div className="d-flex">
                                <div className="roundedCircle" />
                                <div className="roundedCircle" />
                                <div className="roundedCircle" />
                                <div className="roundedCircle" />    
                            </div>
                        </div>
                        <div className="row">
                            <div className="d-flex text-center">
                                <h2 className="title-factory mt-4 ml-4">RANKING DE LA EMPRESA</h2>
                                <h2 className="title-factory mt-4 ml-0">VALORACIÓN DE TUS WORKERS</h2>
                                <h2 className="title-factory mt-4 ml-1">WORKERS ACTIVOS</h2>
                                <h2 className="title-factory mt-4">WORKERS VALORADOS</h2>
                            </div>
                            {/*<p><a onClick={() => this.changePassword()}>Cambiar contraseña</a></p>
                            <p><a onClick={() => this.logout()}>Cerrar sesión</a></p>*/}
                        </div>
                        {/*
                        <div className="col-12 col-md-auto text-left text-md-right">
                            <h4 style={{paddingRight:'27px'}}>Ranking anterior:
                                &nbsp;{this.state.scoring_previous}
                            </h4>
                            <h4>Ranking actual:
                                &nbsp;{this.state.scoring}
                                &nbsp;<i className={scoringArrow}></i>
                            </h4>
                        </div>*/}
                    </div>
                    <div className="row">
                        <div className="col-6 mt-5">
                            <h1 className="h1-factory mt-5">Valoración de la Empresa</h1>
                        </div>
                        <div className="col-6 mt-5 d-flex justify-content-end align-items-end">
                            <div className="calendar-factory">
                                <p className="workers-myaccount__table-header-option-factory">Marzo</p>
                            </div>
                        </div>
                    </div>
                    {/*<div className="row justify-content-center">
                        <div className="col-sd-2 pt-4 text-center">
                            <div className="workers-profile-image" style={
                                {
                                    backgroundImage: `url('${this.state.full_profile_image ? this.state.full_profile_image : ProfilePicture}')`}
                                }>
                                {/* width="90%" */}
                            {/*</div>
                            <div className="upload-profile-wrapper text-right">
                                <UploadProfileFile updateListener={this.updateView.bind(this)}/>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-sd-2 pb-4 text-center">
                            <h1>{this.state.fantasy_name}</h1>
                            <a className="btn btn-lg btn-primary" href='/auth-print' target='_blank'>
                                Autorización
                            </a>
                        </div>
                    </div>*/}
                            {/*
                                this.state.workersListDom.length ?
                                    <article className="container mt-md-2 mb-5 p-3 p-md-5 workers-myaccount">
                                        <div className="row">
                                            <div className="col-sd-2 pb-4"><h2>Mis valoraciones</h2></div>
                                        </div>
                                        {this.state.workersListDom}
                                    </article>
                                : <article className="container mt-md-2 mb-5 p-5 workers-myaccount">
                                        <div className="row">
                                            <div className="col-sd-2 pb-4"><h2>Mis valoraciones</h2></div>
                                        </div>
                                        <p className="text-center">No tienes todavía valoraciones</p>
                                    </article>
                            */}
                        <div className="row">
                            <div className="col-6">
                             <table className="workers-myaccount__tableFactory mt-5" striped bordered hover>
                                <thead className="workers-myaccount__table-header">
                                    <tr>
                                        <th className="workers-myaccount__table-header-option-factory" scope="col">Resumen de altas y bajas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <td className="text-center table-factory-item">2 Workers dados de alta</td>
                                    <tr>
                                        <td className="text-center table-factory-item">5 Workers dados de baja</td>
                                    </tr>
                                </tbody>
                            </table>
                            </div>
                            <div className="col-6">
                             <table className="workers-myaccount__tableFactory mt-5" striped bordered hover>
                                <thead className="workers-myaccount__table-header">
                                    <tr>
                                        <th className="workers-myaccount__table-header-option-factory" scope="col">Estado de Workers Activos</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td className="text-start pl-2 table-factory-item">Construcción en Seco / Primer Oficial</td>
                                    </tr>
                                    <tr>
                                        <td className="text-start pl-2 table-factory-item">Construcción en Seco / Operario</td>
                                        
                                    </tr>
                                    <tr> 
                                        <td className="text-start pl-2 table-factory-item">Plomería / Operario</td>
                                        
                                    </tr>
                                    <tr>
                                        <td className="text-start pl-2 table-factory-item">Pintura / Capataz </td>
                                        
                                    </tr>
                                    <tr>
                                        <td className="text-start pl-2 table-factory-item">Pintura / Operario </td>
                                        
                                    </tr>                                
                                        
                                </tbody>
                            </table>
                            </div>
                        </div>
                </section>
                <section>
                    <div className="row">
                        <div className="col-6 mb-5">
                            <h2 className="title-factory text-center">VELOCIDAD DE TRABAJO</h2>
                            <img src={imgRating} className="img-rating mt-3"></img>
                        </div>
                        <div className="col-6 mb-5">
                            <h2 className="title-factory text-center">RESPONSABILIDAD, ASISTENCIA Y PUNTUALIDAD</h2>
                            <img src={imgRating} className="img-rating mt-3"></img>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mt-5 mb-5">
                            <h2 className="title-factory text-center">CONOCIMIENTO TÉCNICO Y CÁLIDAD</h2>
                            <img src={imgRating} className="img-rating mt-3"></img>
                        </div>
                        <div className="col-6 mt-5 mb-5">
                            <h2 className="title-factory text-center"> RESPETO Y MANEJO DE LAS FORMAS</h2>
                            <img src={imgRating} className="img-rating mt-3"></img>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6 mt-5 text-qualify-factory">
                            <h2 className="title-factory text-center">ORGANIZACIÓN Y TRABAJO EN EQUIPO</h2>
                            <img src={imgRating} className="img-rating mt-3"></img>
                        </div>
                    </div>
                </section>
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                       className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
            </div>
        );
    }
}

//export default MyAccount;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToAuthorizationPrint: () => push('/auth-print'),
    goToLogin: () => push('/login')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(MyFactory)