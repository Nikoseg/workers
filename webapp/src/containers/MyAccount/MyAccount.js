import React, { Component } from 'react';
import Rodal from 'rodal';

// TODO: Revisar...
import 'rodal/lib/rodal.css';

import './../../modules/authService';

//ROUTER
import { push } from 'connected-react-router'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AuthService from "../../modules/authService";

// import ProfilePicture from './../../static/img/profile_picture.png';
// import UploadProfileFile from './../../components/uploadProfileFile/uploadProfileFile';
import IconSearch from './../../static/img/icon-search.png'
import QualificationListProfile from "../../components/qualificationListProfile/qualificationListProfile";
import ChangePassword from "../../components/changePassword/changePassword";

// API SERVICE
import APIService from './../../modules/apiService';

class MyAccount extends Component {

    constructor(props) {
        super(props);

        this.state = {
            scoring_previous: '0.0',
            scoring: '0.0',
            workers: [],
            workersListDom: [],
        };

        this.auth = new AuthService();
        this.api = new APIService();
    }

    logout() {
        this.auth.logout(); // clears user data
        this.auth.setHeaderInOrderOfUserStatus();
        this.props.goToLogin();
    }

    updateView() {
        this.componentDidMount();
    }

    eraseEmptyWorkers(workers){
        const filteredWorkers = workers.filter((worker) => worker.qualifications && worker.qualifications.length > 0);
        this.renderWorkers(filteredWorkers)
    }

    renderWorkers(workers) {
        if(!workers) {
            return;
        }
        let workersListDom = workers.map((worker, index) =>
            <QualificationListProfile
                index={index}
                key={worker.id + Math.random()}
                id={worker.id + Math.random()}
                worker={worker}
                updateView={this.updateView.bind(this)}
            />
        );
        this.setState({
            workersListDom
        });
    }

    updateCompanyData() {
        this.api.getMyCompanies().then((res)=>{
            let mergedObj = Object.assign({}, this.state, res.data);
            this.setState(mergedObj);
            this.eraseEmptyWorkers(mergedObj.workers);
        }).catch((err)=>{
            //this.showModal('Error', err.message);
            console.log(err.message);
        });
    }

    // @TODO: Implementar
    componentDidMount() {
        this.api.getUserMe()
            .then((res)=>{
                delete res.data.full_profile_image; // Avoid to step a valid image with a null uri
                let mergedObj = Object.assign({}, this.state, res.data);
                this.setState(mergedObj);
                this.updateCompanyData();
            }).catch((err)=>{
                this.showModal('Error', err.message);
            });
    }

    changePassword() {
        this.showModal('Cambiar contraseña', <ChangePassword/>);
    }

    /* MODAL Functions */
    showModal(title, content) {
        this.setState({ modalTitle: title });
        this.setState({ modalContent: content });
        this.show();
    }

    show() {
        this.setState({ visible: true });
    }

    hide() {
        this.setState({ visible: false });
    }
    //

    getScoringArrow() {
        const upArrow = 'fa fa-arrow-up text-primary';
        const downArrow = 'fa fa-arrow-down text-danger';
        const equalArrow = 'fa fa-arrow-right';

        if(this.state.scoring < this.state.scoring_previous) {
            return downArrow;
        }
        if(this.state.scoring > this.state.scoring_previous) {
            return upArrow;
        }
        if(this.state.scoring === this.state.scoring_previous) {
            return equalArrow;
        }
        return equalArrow; // default
    }

    // TODO: Revisar después esto...
    getRodalSize() {
        debugger;
        let size = '';
        if(window && window.innerWidth) {
            const windowWidth = window.innerWidth;
            if(windowWidth < 800) {
                size = '100%';
            }
        }

        return size;

    }

    render() {
        const equalArrow = 'fa fa-arrow-right';

        const controlsClassname = 'btn btn-large btn-lg btn-primary';

        let scoringArrow = equalArrow;
        scoringArrow = this.getScoringArrow();

        return (
            <div>
                {/* <section className="container mb-5 p-lg-5 workers-myaccount">
                    <div className="row mb-5">
                        <div className="col">
                            <h1>Mi Cuenta</h1>
                            <p><a onClick={() => this.changePassword()}>Cambiar contraseña</a></p>
                            <p><a onClick={() => this.logout()}>Cerrar sesión</a></p>
                        </div>
                        <div className="col-12 col-md-auto text-left text-md-right">
                            <h4 style={{paddingRight:'27px'}}>Ranking anterior:
                                &nbsp;{this.state.scoring_previous}
                            </h4>
                            <h4>Ranking actual:
                                &nbsp;{this.state.scoring}
                                &nbsp;<i className={scoringArrow}></i>
                            </h4>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-sd-2 pt-4 text-center">
                            <div className="workers-profile-image" style={
                                {
                                    backgroundImage: `url('${this.state.full_profile_image ? this.state.full_profile_image : ProfilePicture}')`}
                                }>
                                width="90%"
                            </div>
                            <div className="upload-profile-wrapper text-right">
                                <UploadProfileFile updateListener={this.updateView.bind(this)}/>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center">
                        <div className="col-sd-2 pb-4 text-center">
                            <h1>{this.state.fantasy_name}</h1>
                            <a className="btn btn-lg btn-primary" href='/auth-print' target='_blank'>
                                Autorización
                            </a>
                        </div>
                    </div> */}
                            {
                                this.state.workersListDom.length ?
                                    <article className="workers-myaccount">
                                        <div className="row p-0 m-0 pl-3 mb-4">
                                            <div className="col-sm-12 col-md-8 p-0 d-flex align-items-center">
                                                <h1 className="font-weight-bold">Mis Valoraciones</h1>
                                            </div>
                                            <div className="col-sm-12 col-md-4 p-0 d-flex justify-content-end">
                                                <a className="btn btn-lg btn-primary d-flex align-items-center" href='/qualify'>
                                                    Nueva calificación
                                                </a>
                                                <a className="btn workers-myaccount__search-icon" href='/qualify'>
                                                    <img width="44" height="44" src={IconSearch} />
                                                </a>
                                            </div>
                                        </div>
                                        <div className="row p-0 m-0 mb-4 pl-3">
                                            <h4 className="workers-myaccount__text--light cursor-pointer separator">Exportar listado de Workers |</h4>
                                            <h4 className="workers-myaccount__text cursor-pointer">Subir listado de Workers</h4>
                                        </div>
                                        <table className="workers-myaccount__table">
                                            <thead className="workers-myaccount__table-header">
                                                <tr>
                                                    <th className="workers-myaccount__table-header-option" scope="col">Valoración</th>
                                                    <th className="workers-myaccount__table-header-option" scope="col">Nombre</th>
                                                    <th className="workers-myaccount__table-header-option" scope="col">Apellido</th>
                                                    <th className="workers-myaccount__table-header-option" scope="col">CUIT</th>
                                                    <th className="workers-myaccount__table-header-option" scope="col">Editar</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.workersListDom}
                                            </tbody>
                                        </table>
                                    </article>
                                : <article className="container mt-md-2 mb-5 p-5 workers-myaccount">
                                        <div className="row">
                                            <div className="col-sd-2 pb-4"><h2>Mis valoraciones</h2></div>
                                        </div>
                                        <p className="text-center">No tienes todavía valoraciones</p>
                                    </article>
                            }
                {/* </section> */}
                <Rodal visible={this.state.visible} onClose={this.hide.bind(this)}
                       className="workers-scroll-content workers-scroll-content__controls">
                    <h4 className="mt-4 mb-4">{this.state.modalTitle}</h4>
                    <div>{this.state.modalContent}</div>
                </Rodal>
            </div>
        );
    }
}

//export default MyAccount;

const mapDispatchToProps = dispatch => bindActionCreators({
    goToAuthorizationPrint: () => push('/auth-print'),
    goToLogin: () => push('/login')
}, dispatch);

export default connect(
    null,
    mapDispatchToProps
)(MyAccount)